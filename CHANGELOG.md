# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.2.1] - 2024-09-26

- [#28111] bug fixed "default value" when a field is optional on `Time_Interval` type

## [v2.2.0] - 2024-05-17

#### Enhancements

- [#27488] In edit mode allowed to erase (not mandatory) fields according to data type
- [#27627] Moved logs at TRACE level

## [v2.1.1] - 2024-03-19

#### Enhancements

- [#26656] Aligned the data-model to ckan-metadata-publisher-widget 

## [v2.1.0] - 2023-02-01

#### Enhancements

- [#23188] Improved Legend "is required field"
- [#24515] Managed the files already uploaded

## [v2.0.0] - 2022-11-14

#### Enhancements

- [#23188] Overloaded the method getProfilesInTheScope(forName)
- [#22890] Including the set/get currentValue for the "Update" facility
- [#23188] Advanced the MultipleDilaogUpload with a more complex object
- [#23544] Integrated with the fieldId added to gCube Metadata Profile
- [#24111] Added dependency required for building with JDK_11
- Moved to GWT 2.9
- Moved to maven-portal-bom 3.6.4


## [v1.0.1-SNAPSHOT] - 2020-10-08

#### Bug fixes

- [#20446] Catalogue Publishing Widget: field value unexpectedly added in case of optional field 


## [v1.0.0] - 2020-10-08

#### First release

- [#19884] Create widget to build a data-entry form by using metadata-profile-discovery 

- [#19878] Create a data entry facility to get (meta)data object defined by "gCube Metada Profile"