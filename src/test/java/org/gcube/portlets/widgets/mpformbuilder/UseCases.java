//package org.gcube.portlets.widgets.mpformbuilder;
//
//import java.util.Arrays;
//import java.util.List;
//
//import org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderServiceAsync;
//import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm;
//import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
//
//import com.google.gwt.event.shared.HandlerManager;
//import com.google.gwt.user.client.rpc.AsyncCallback;
//
//public class UseCases {
//	
//	String scope = "/gcube/devsec/devVRE";
//	String secondaryType = "GeoNaMetadata";
//	private final HandlerManager eventBus = new HandlerManager(null);
//	
//	public UseCases() {
//		
//		method1();
//		
//		method2();
//	}
//
//	private void method2() {
//		
//		MetadataProfileFormBuilderServiceAsync.Util.getInstance().getProfilesInTheScope(scope, secondaryType, new AsyncCallback<List<MetaDataProfileBean>>() {
//			
//			@Override
//			public void onSuccess(List<MetaDataProfileBean> result) {
//				
//				for (MetaDataProfileBean metaDataProfileBean : result) {
//					CreateMetadataForm baseForm = new CreateMetadataForm(Arrays.asList(metaDataProfileBean), eventBus);
//					//TODO
//					//ADD this to main panel
//				}
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//		});
//	}
//
//	private void method1() {
//		
//		//TODO
//		//ADD this to main panel
//		CreateMetadataForm baseForm = new CreateMetadataForm(scope, secondaryType, eventBus);
//		
//	}
//
//}
