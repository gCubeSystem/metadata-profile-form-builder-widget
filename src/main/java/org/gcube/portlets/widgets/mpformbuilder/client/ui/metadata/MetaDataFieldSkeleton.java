package org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.mpformbuilder.client.events.CloseCreationFormProfileEvent;
import org.gcube.portlets.widgets.mpformbuilder.client.events.CloseCreationFormProfileEventHandler;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm.OPERATION;
import org.gcube.portlets.widgets.mpformbuilder.client.openlayerwidget.GeoJsonAreaSelectionDialog;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.timeandranges.DataTimeBox;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.DialogUpload;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.MultipleDilaogUpload;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.GcubeDialogExtended;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.FieldAsGroup;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.FieldAsTag;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.TaggingGroupingValue;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ControlLabel;
import com.github.gwtbootstrap.client.ui.Controls;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.Popover;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.ResizeType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class MetaDataFieldSkeleton.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 20, 2024
 */
public class MetaDataFieldSkeleton extends Composite {

	private static MetaDataFieldSkeletonUiBinder uiBinder = GWT.create(MetaDataFieldSkeletonUiBinder.class);

	/**
	 * The Interface MetaDataFieldSkeletonUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         May 20, 2024
	 */
	interface MetaDataFieldSkeletonUiBinder extends UiBinder<Widget, MetaDataFieldSkeleton> {
	}

	@UiField
	Element mandatorySymbol;
	@UiField
	SpanElement name;
	@UiField
	FlowPanel elementPanel;
	@UiField
	FlowPanel noteFieldContainer;
	@UiField
	Popover noteFieldPopover;
	@UiField
	ControlLabel controlLabel;
	@UiField
	Controls controls;
	@UiField
	Icon infoIcon;
	@UiField
	FocusPanel focusPanelIconContainer;
	@UiField
	ControlGroup metafieldControlGroup;

	@UiField
	Button eraseButton;
	@UiField
	FlowPanel eraseFieldContainer;

	// private static final String REGEX_IS_NUMBER = "[0-9]+[.]?[0-9]+";

	// the element that holds the value (it could be a checkbox, textbox or listbox,
	// textarea, calendar, two calendars, more calendars)
	private Widget holder;

	// the field this object represents
	private MetadataFieldWrapper field;

	// the dialog box for this metadata
	private GcubeDialogExtended dialog;

	// range list
	private List<DataTimeBox> rangesList = new ArrayList<DataTimeBox>();

	private List<TextArea> geoJsonList = new ArrayList<TextArea>();

	// save event bus reference
	private HandlerManager eventBus;

	// errors
	private static final String MANDATORY_ATTRIBUTE_MISSING = "a mandatory attribute cannot be empty";
	private static final String MALFORMED_ATTRIBUTE = " the inserted value has a wrong format";
	private static final String ADD_NEW_TIME_RANGE = "Add a new Time Range";
	private static final String DELETE_TIME_RANGE = "Delete the last Time Range";

	// missing range value
	private static final String INSERT_MISSING_VALUE = " you cannot specify an end date without a start one";
	private static final String INSERT_MISSING_VALUE_MANDATORY = " one or more range value missing in mandatory attribute";
	private static final String UPPER_RANGE_NOT_SPECIFIED = "Not specified";
	// time range separator
	public static final String RANGE_SEPARATOR = ",";
	private static final String TOOLTIP_MULTISELECTION = "Hold down the Control (CTRL) or Command (CMD) button to select multiple options";

	private static final String UPLOAD_MISSING_FILE = "You must upload a file";

	/**
	 * Instantiates a new meta data field skeleton.
	 *
	 * @param field     the field
	 * @param eventBus  the event bus
	 * @param operation the operation
	 * @throws Exception the exception
	 */
	public MetaDataFieldSkeleton(final MetadataFieldWrapper field, HandlerManager eventBus, OPERATION operation)
			throws Exception {
		initWidget(uiBinder.createAndBindUi(this));

		// prepare information
		this.field = field;
		// event bus
		this.eventBus = eventBus;

		eraseFieldContainer.setVisible(false);
		eraseButton.setIcon(IconType.BAN_CIRCLE);

		// bind
		bind();

		if (operation == null)
			operation = OPERATION.NEW;

		switch (field.getType()) {

		case Boolean:

			// its a checkbox
			holder = new CheckBox();
			if (field.getDefaultValue() != null)
				((CheckBox) holder).setValue(Boolean.valueOf(field.getDefaultValue()));

			if (operation.equals(OPERATION.UPDATE)) {
				try {
					if (field.getCurrentSingleValue() != null)
						((CheckBox) holder).setValue(Boolean.valueOf(field.getDefaultValue()));
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			break;

		case GeoJSON:
			// MANAGED By FRANCESCO
			final VerticalPanel containerGeoJSON = new VerticalPanel();
			// containerGeoJSON.setWidth("100%");

			final TextArea textArea = new TextArea();
			textArea.setWidth("100%");
			textArea.setResize(ResizeType.VERTICAL);

			if (field.getDefaultValue() != null)
				textArea.setText(field.getDefaultValue());

			if (operation.equals(OPERATION.UPDATE)) {
				try {
					if (field.getCurrentSingleValue() != null)
						textArea.setText((String) field.getCurrentSingleValue());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			containerGeoJSON.add(textArea);

			HorizontalPanel hp = new HorizontalPanel();
			hp.getElement().getStyle().setMarginTop(10, Unit.PX);
			final ListBox geometries = GeoJsonAreaSelectionDialog.getGeometries();

			Button butt = new Button();
			butt.setIcon(IconType.MAP_MARKER);
			butt.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					String geometrySelected = geometries.getSelectedItemText();
					if (geometrySelected == null) {
						containerGeoJSON.add(new Alert("Please, select one geometry", AlertType.WARNING, true));
					}

					final GeoJsonAreaSelectionDialog dialog = new GeoJsonAreaSelectionDialog(
							GeoJsonAreaSelectionDialog.toGeometry(geometrySelected));

					Command fillGeoJSONArea = new Command() {
						public void execute() {

							try {
								String geoJsonGeom = dialog.getWKTToGeoJSON();
								if (geoJsonGeom != null)
									textArea.setText(geoJsonGeom);
								else {
									textArea.setText("");
									containerGeoJSON.add(new Alert("Sorry, an error occurred by reading the Geometry",
											AlertType.ERROR, true));
								}
							} catch (Exception e) {
								containerGeoJSON.add(new Alert(e.getMessage(), AlertType.ERROR, true));
							}
						}
					};

					dialog.fireCommandOnResponse(fillGeoJSONArea);
					dialog.center();
				}
			});

			geoJsonList.add(textArea);
			Label label = new Label("Draw a");
			label.getElement().getStyle().setMarginRight(5, Unit.PX);
			hp.add(label);
			geometries.getElement().getStyle().setMarginRight(5, Unit.PX);
			hp.add(geometries);
			hp.add(butt);
			geometries.setItemSelected(0, true);
			containerGeoJSON.add(hp);
			holder = containerGeoJSON;

			break;

		case Text:

			holder = new TextArea();
			TextArea txtArea = ((TextArea) holder);
			txtArea.setResize(ResizeType.VERTICAL);

			if (field.getDefaultValue() != null)
				txtArea.setText(field.getDefaultValue());

			if (operation.equals(OPERATION.UPDATE)) {
				try {
					if (field.getCurrentSingleValue() != null)
						txtArea.setText((String) field.getCurrentSingleValue());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			break;

		case Time:

			DataTimeBox ref;
			holder = ref = new DataTimeBox(false);

			// set time, if present
			if (field.getDefaultValue() != null && !field.getDefaultValue().isEmpty()) {
				String[] dateAndTime = field.getDefaultValue().split(" ");
				if (dateAndTime.length > 0) {
					ref.setStartDate(dateAndTime[0], dateAndTime.length > 1 ? dateAndTime[1] : null);
				}
			}

			if (operation.equals(OPERATION.UPDATE)) {
				// set time, if present
				try {
					if (field.getCurrentSingleValue() != null) {
						String currentValue = (String) field.getCurrentSingleValue();
						String[] dateAndTime = currentValue.split(" ");
						if (dateAndTime.length > 0) {
							ref.setStartDate(dateAndTime[0], dateAndTime.length > 1 ? dateAndTime[1] : null);
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			break;

		case Time_Interval:

			DataTimeBox rangeBox;
			holder = rangeBox = new DataTimeBox(true);
			setRangeTimeInTimeBox(field.getDefaultValue(), rangeBox);
			if (operation.equals(OPERATION.UPDATE)) {
				// set time, if present
				try {
					setRangeTimeInTimeBox((String) field.getCurrentSingleValue(), rangeBox);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			rangesList.add(rangeBox);
			break;

		case Times_ListOf:

			holder = new FlowPanel();

			// start and end range date
			final VerticalPanel containerRanges = new VerticalPanel();
			containerRanges.setWidth("100%");

			SimplePanel panelFirstRange = new SimplePanel();
			DataTimeBox rangeBoxFirst = new DataTimeBox(true);
			setRangeTimeInTimeBox(field.getDefaultValue(), rangeBoxFirst);

			if (operation.equals(OPERATION.UPDATE)) {
				try {
					setRangeTimeInTimeBox((String) field.getCurrentSingleValue(), rangeBoxFirst);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			panelFirstRange.add(rangeBoxFirst);
			rangesList.add(rangeBoxFirst);

			// Add more button
			Button addRangeButton = new Button();
			addRangeButton.setIcon(IconType.PLUS_SIGN);
			addRangeButton.setTitle(ADD_NEW_TIME_RANGE);

			addRangeButton.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {

					final FlowPanel newRange = new FlowPanel();
					final DataTimeBox newRangeBox = new DataTimeBox(true);
					setRangeTimeInTimeBox(field.getDefaultValue(), newRangeBox);
					rangesList.add(newRangeBox);

					// delete button
					Button deleteRangeButton = new Button("", IconType.MINUS_SIGN, new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {

							newRange.removeFromParent();
							rangesList.remove(newRangeBox);
						}
					});
					deleteRangeButton.setTitle(DELETE_TIME_RANGE);
					newRange.add(newRangeBox);
					newRange.add(deleteRangeButton);
					containerRanges.add(newRange);

				}
			});

			// add calendars and plus sign
			containerRanges.add(panelFirstRange);

			// add the vertical panel first, then the button
			((FlowPanel) holder).add(containerRanges);
			((FlowPanel) holder).add(addRangeButton);

			break;

		case Number:

			holder = new TextBox();

			if (field.getDefaultValue() != null)
				((TextBox) holder).setText(field.getDefaultValue());

			if (operation.equals(OPERATION.UPDATE)) {
				try {
					if (field.getCurrentSingleValue() != null)
						((TextBox) holder).setText((String) field.getCurrentSingleValue());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			break;

		case File:

			holder = new MultipleDilaogUpload(field.getFieldName());

			break;

		case String:

			// it could be a listbox or a textbox according to the vocabulary fields
			if (field.getVocabulary() == null || field.getVocabulary().isEmpty()) {

				// textbox
				holder = new TextBox();

				if (field.getDefaultValue() != null)
					((TextBox) holder).setText(field.getDefaultValue());

				if (operation.equals(OPERATION.UPDATE)) {
					try {
						if (field.getCurrentSingleValue() != null)
							((TextBox) holder).setText((String) field.getCurrentSingleValue());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

			} else {

				// listbox
				ListBox tempListBox;
				holder = tempListBox = new ListBox(field.isMultiSelection());

				if (field.isMultiSelection())
					tempListBox.setTitle(TOOLTIP_MULTISELECTION);

				// if it is not mandatory and not multi-selection, add a disabled option
				// (placeholder)
				if (!field.getMandatory() && !field.isMultiSelection()) {
					listBoxSelectPlaceholder(tempListBox);
				}

				// get vocabulary fields
				List<String> vocabulary = field.getVocabulary();

				for (String term : vocabulary) {
					tempListBox.addItem(term, term);
				}

				// set default value
				if (field.getDefaultValue() != null)
					tempListBox.setSelectedValue(field.getDefaultValue());

				// to be sure
				tempListBox.setMultipleSelect(field.isMultiSelection());

				if (operation.equals(OPERATION.UPDATE)) {
					try {
						if (field.getCurrentValues() != null) {
							if (field.isMultiSelection()) {
								// Buggy in Bootstrap
								for (String value : field.getCurrentValues()) {
									GWT.log("Multiple Selecting: " + value);
									tempListBox.setSelectedValue((String) value);
								}
							} else {
								GWT.log("Selecting: " + field.getCurrentSingleValue());
								tempListBox.setSelectedValue((String) field.getCurrentSingleValue());
							}

						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

			}

			break;

		default:
			return;

		}

		try {
			ChangeHandler handler = new ChangeHandler() {

				@Override
				public void onChange(ChangeEvent event) {
					GWT.log("Fired is field value");
					removeError();
					isFieldValueValid();

				}
			};
			holder.addDomHandler(handler, ChangeEvent.getType());
		} catch (Exception e) {
			// TODO: handle exception
		}

		// add custom css properties
		controls.addStyleName("form-controls-custom");
		controlLabel.addStyleName("form-control-label-custom");

		// save the name
		name.setInnerText(field.getFieldName() + ":");

		// check if it is mandatory
		if (!field.getMandatory())
			mandatorySymbol.getStyle().setDisplay(Display.NONE);

		// add to the elementPanel
		elementPanel.insert(holder, 0);

		// set holder width
		if (holder.getClass().equals(ListBox.class))
			holder.setWidth("99%");
		else
			holder.setWidth("97%");

		// If the field is erasable views the erase panel
		if (field.isErasableField()) {
			eraseFieldContainer.setVisible(true);
		}

		// set the notes, if any, and the popover
		if (field.getNote() != null && !field.getNote().isEmpty()) {
			noteFieldPopover.setText(new HTML("<p style='color:initial'>" + field.getNote() + "</p>").getHTML());
			noteFieldPopover.setHeading(new HTML("<b>" + field.getFieldName() + "</b>").getHTML());
			infoIcon.getElement().getStyle().setCursor(Cursor.HELP);
			noteFieldPopover.setHtml(true);
			noteFieldContainer.setVisible(true);
		} else {
			noteFieldContainer.setVisible(false);
		}

		// add a resize handler to center the dialog box if it's not null
		Window.addResizeHandler(new ResizeHandler() {

			@Override
			public void onResize(ResizeEvent event) {

				if (dialog != null)
					dialog.center();

			}
		});

	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Widget getHolder() {
		return holder;
	}

	/**
	 * Bind on events.
	 */
	private void bind() {

		// on close form
		eventBus.addHandler(CloseCreationFormProfileEvent.TYPE, new CloseCreationFormProfileEventHandler() {

			@Override
			public void onClose(CloseCreationFormProfileEvent event) {

				if (dialog != null)
					dialog.hide();

			}
		});

		eraseButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				erase();

			}
		});

	}

	/**
	 * On info icon click.
	 *
	 * @param c the c
	 */
	@UiHandler("focusPanelIconContainer")
	void onInfoIconClick(ClickEvent c) {

		if (dialog == null) {

			// create the dialog box
			dialog = new GcubeDialogExtended(field.getFieldName(), field.getNote());

			// set as non modal
			dialog.setModal(false);
		}

		// else just show and center
		dialog.center();
		dialog.show();
	}

	/**
	 * Check if this field has valid values.
	 *
	 * @return a string with the occurred error on error, null otherwise
	 */
	public String isFieldValueValid() {

		switch (field.getType()) {

		case Boolean:

			// nothing to validate
			return null;

		case Text:
		case GeoJSON:
			String textAreaValue = getFieldCurrentValue().get(0);

			if (field.getMandatory()) {

				if (!textAreaValue.trim().isEmpty())
					if (field.getValidator() == null || field.getValidator().isEmpty())
						return null; // no further check
					else
						return checkValidator(textAreaValue, field.getValidator()) ? null : MALFORMED_ATTRIBUTE;
				else
					return MANDATORY_ATTRIBUTE_MISSING;

			} else {

				if (textAreaValue.trim().isEmpty())
					return null;

				else
					return checkValidator(textAreaValue, field.getValidator()) ? null : MALFORMED_ATTRIBUTE;

			}

		case Time:

			String dateValue = getFieldCurrentValue().get(0);

			if (field.getMandatory()) {
				if (dateValue.isEmpty())
					return MANDATORY_ATTRIBUTE_MISSING;
			}

			return null;

		case Time_Interval:

			String rangeValue = rangesList.get(0).getCurrentValue();

			if (field.getMandatory()) {
				if (rangeValue.contains(DataTimeBox.MISSING_RANGE_VALUE_START)
						|| rangeValue.contains(DataTimeBox.MISSING_RANGE_VALUE_END))
					return INSERT_MISSING_VALUE_MANDATORY;
			}

			if (rangeValue.contains(DataTimeBox.MISSING_RANGE_VALUE_START)
					&& !rangeValue.equals(DataTimeBox.MISSING_RANGE_VALUE_START + DataTimeBox.RANGE_SEPARATOR_START_END
							+ DataTimeBox.MISSING_RANGE_VALUE_END))
				return INSERT_MISSING_VALUE;

			return null;

		case Times_ListOf:

			for (DataTimeBox el : rangesList) {

				String currentValue = el.getCurrentValue();
				if (field.getMandatory()) {
					if (currentValue.contains(DataTimeBox.MISSING_RANGE_VALUE_START)
							|| currentValue.contains(DataTimeBox.MISSING_RANGE_VALUE_END))
						return INSERT_MISSING_VALUE_MANDATORY;
				}

				GWT.log("Printing " + currentValue);
				if (currentValue.contains(DataTimeBox.MISSING_RANGE_VALUE_START)
						&& !currentValue.equals(DataTimeBox.MISSING_RANGE_VALUE_START
								+ DataTimeBox.RANGE_SEPARATOR_START_END + DataTimeBox.MISSING_RANGE_VALUE_END))
					return INSERT_MISSING_VALUE;

			}
			return null;

		case Number:

			String numberValue = ((TextBox) holder).getValue();

			if (field.getMandatory()) {

				if (!numberValue.trim().isEmpty())
					if (field.getValidator() == null || field.getValidator().isEmpty())
						return isANumber(numberValue) ? null : MALFORMED_ATTRIBUTE;
					else
						return checkValidator(numberValue, field.getValidator()) ? null : MALFORMED_ATTRIBUTE;
				else
					return " a mandatory attribute cannot be empty";

			} else {

				if (numberValue.trim().isEmpty())
					return null;
				else {
					String validatorToUse = field.getValidator();
					if (validatorToUse != null && !validatorToUse.isEmpty())
						return checkValidator(numberValue, validatorToUse) ? null : MALFORMED_ATTRIBUTE;
					else
						return isANumber(numberValue) ? null : MALFORMED_ATTRIBUTE;
				}
			}

		case String:

			// just handle the case of textbox
			if (holder.getClass().equals(TextBox.class)) {

				String textBoxValue = getFieldCurrentValue().get(0);
				if (field.getMandatory()) {
					if (!textBoxValue.trim().isEmpty())
						if (field.getValidator() == null || field.getValidator().isEmpty())
							return null; // no further check
						else
							return checkValidator(textBoxValue, field.getValidator()) ? null : MALFORMED_ATTRIBUTE;
					else
						return MANDATORY_ATTRIBUTE_MISSING;

				} else {
					if (textBoxValue.trim().isEmpty())
						return null;
					else
						return checkValidator(textBoxValue, field.getValidator()) ? null : MALFORMED_ATTRIBUTE;
				}
			} else {

				List<String> listboxValues = getFieldCurrentValue();

				if (listboxValues.isEmpty() && field.getMandatory())
					return MANDATORY_ATTRIBUTE_MISSING;

				for (String value : listboxValues) {
					if (!field.getMandatory()) {

						if (field.getValidator() == null || field.getValidator().isEmpty())
							continue;
						else if (checkValidator(value, field.getValidator()))
							continue;
						else
							return MALFORMED_ATTRIBUTE;

					} else {

						if (value == null || value.isEmpty())
							return MANDATORY_ATTRIBUTE_MISSING;
						else
							continue;

					}
				}
			}

			return null;

		case File:

			GWT.log("Checking is valid File");
			if (field.getMandatory()) {
				if (holder.getClass().equals(DialogUpload.class)
						|| holder.getClass().equals(MultipleDilaogUpload.class)) {
					DialogUpload dUpload = (DialogUpload) holder;
					if (dUpload.getFileUploadingState() != null && dUpload.getFileUploadingState().getFile() != null) {
						return dUpload.getFileUploadingState().getFile().getTempSystemPath() != null ? null
								: UPLOAD_MISSING_FILE;
					}
				}
				return UPLOAD_MISSING_FILE;

			}
			return null;

		default:
			return null;

		}
	}

	/**
	 * Check if this string is a number.
	 *
	 * @param numberValue the number value
	 * @return true, if is a number
	 */
	private boolean isANumber(String numberValue) {
		return numberValue.matches("-?\\d+(\\.\\d+)?");
	}

	/**
	 * Check if value matches validator (regex). In case validator is null, true is
	 * returned.
	 *
	 * @param value     the value
	 * @param validator the validator
	 * @return true if validator is null OR value.matches(reges), false otherwise
	 */
	private boolean checkValidator(String value, String validator) {
		GWT.log("Value is " + value);
		GWT.log("Validator is " + validator);
		if (validator == null || validator.isEmpty())
			return true;
		else
			return value.matches(validator);
	}

	/**
	 * Returns the current value of the field. In case of TimeInterval or TimeList
	 * see getTimeIntervalOrTimeListWithoutMissing()
	 *
	 * @return the field current value
	 */
	public List<String> getFieldCurrentValue() {

		List<String> toReturn = new ArrayList<String>();
		String manipulatedTemp = "";

		switch (field.getType()) {

		case Boolean:

			toReturn.add(((CheckBox) holder).getValue().toString());
			break;

		case Text:
			toReturn.add(((TextArea) holder).getText());
			break;

		case GeoJSON:
			toReturn.add(geoJsonList.get(0).getText());
			break;

		case Time:

			toReturn.add(
					((DataTimeBox) holder).getCurrentValue().replaceAll(DataTimeBox.MISSING_RANGE_VALUE_START, "")); // it
																														// was
																														// a
																														// noRange
																														// metadata
			break;

		case Time_Interval:

			manipulatedTemp = rangesList.get(0).getCurrentValue().replaceAll(DataTimeBox.MISSING_RANGE_VALUE_START, "")
					.replaceAll(DataTimeBox.MISSING_RANGE_VALUE_END, UPPER_RANGE_NOT_SPECIFIED);

			// fixed by Francesco, see #20663#note-12 and #28111
			if (!field.getMandatory()) {
				if (manipulatedTemp.equalsIgnoreCase(DataTimeBox.RANGE_SEPARATOR_START_END + UPPER_RANGE_NOT_SPECIFIED))
					manipulatedTemp = "";
			}

			// split to check if the extreme are equals
			if (!manipulatedTemp.isEmpty()) {
				String[] temp = manipulatedTemp.split(DataTimeBox.RANGE_SEPARATOR_START_END);
				if (temp[0].equals(temp[1]))
					manipulatedTemp = temp[0];
			}

			toReturn.add(manipulatedTemp);
			break;

		case Times_ListOf:

			manipulatedTemp = "";
			for (DataTimeBox elem : rangesList) {

				String currentRange = elem.getCurrentValue().replaceAll(DataTimeBox.MISSING_RANGE_VALUE_START, "")
						.replaceAll(DataTimeBox.MISSING_RANGE_VALUE_END, UPPER_RANGE_NOT_SPECIFIED);
				if (currentRange.equals(DataTimeBox.RANGE_SEPARATOR_START_END + UPPER_RANGE_NOT_SPECIFIED))
					continue;

				String[] splitted = currentRange.split(DataTimeBox.RANGE_SEPARATOR_START_END);
				if (splitted[0].equals(splitted[1]))
					manipulatedTemp += manipulatedTemp.isEmpty() ? splitted[0] : RANGE_SEPARATOR + splitted[0];
				else
					manipulatedTemp += manipulatedTemp.isEmpty()
							? splitted[0] + DataTimeBox.RANGE_SEPARATOR_START_END + splitted[1]
							: RANGE_SEPARATOR + splitted[0] + DataTimeBox.RANGE_SEPARATOR_START_END + splitted[1];

			}

			if (manipulatedTemp.endsWith(RANGE_SEPARATOR))
				manipulatedTemp = manipulatedTemp.substring(0, manipulatedTemp.length() - 1);

			toReturn.add(manipulatedTemp);
			break;

		case Number:
		case String:

			if (holder.getClass().equals(TextBox.class))
				toReturn.add(((TextBox) holder).getText());
			else {

				// handle single and multi-selected case
				for (int i = 0; i < ((ListBox) holder).getItemCount(); i++) {
					if (((ListBox) holder).isItemSelected(i)) {
						toReturn.add(((ListBox) holder).getItemText(i));
					}
				}

				// if it was not mandatory but there was no choice, returning empty string
				if (!field.getMandatory()) {
					// Task #20446 - bug fix. Ignoring the placeholder
					if (toReturn.size() == 1) {
						String placeholder = "Select " + field.getFieldName();
						if (toReturn.get(0).equals(placeholder)) {
							GWT.log("Skipping placeholder: " + placeholder);
							toReturn.clear();
							// toReturn.add("");
						}
					}
				}
			}

			break;
		case File:

			if (holder.getClass().equals(MultipleDilaogUpload.class) || holder.getClass().equals(DialogUpload.class)) {
				DialogUpload dUpload = (DialogUpload) holder;
				if (dUpload.getFileUploadingState() != null) {
					String filePath = dUpload.getFileUploadingState().getFile().getTempSystemPath();
					toReturn.add(filePath);
				} else
					toReturn.add(null);
			}

			break;

		default:
			break;

		}

		return toReturn;
	}

	/**
	 * Returns the current name of the field.
	 *
	 * @return the field name original
	 */
	public String getFieldNameOriginal() {

		return field.getFieldName();

	}

	/**
	 * Returns the current name of the field.
	 *
	 * @return the field name qualified
	 */
	public String getFieldNameQualified() {

		// see https://support.d4science.org/issues/23544
		if (field.getFieldId() != null) {
			return field.getFieldId();
		}

		// Using the getFieldNameFromCategory if the category is present in the model as
		// cateogory:fieldName
		if (field.getFieldNameFromCategory() != null
				&& field.getFieldNameFromCategory().compareTo(field.getFieldName()) != 0) {
			return field.getFieldNameFromCategory();
		}

		return field.getFieldName();

	}

	/**
	 * Erase the field. Reset the field
	 */
	private void erase() {

		if (field.isErasableField()) {

			switch (field.getType()) {

			case Boolean:

				((CheckBox) holder).setValue(false);
				break;

			case GeoJSON:
			case Text:

				((TextArea) holder).setText("");
				break;

			case Time:

				DataTimeBox dtbox = ((DataTimeBox) holder);
				dtbox.setStartDate(null, null);
				dtbox.setEndDate(null, null);
				break;

			case Time_Interval:

				DataTimeBox dtboxTI = rangesList.get(0);
				dtboxTI.setStartDate(null, null);
				dtboxTI.setEndDate(null, null);
				break;

			case Times_ListOf:

				for (DataTimeBox el : rangesList) {
					el.setStartDate(null, null);
					el.setEndDate(null, null);
				}

				break;

			case Number:

				((TextBox) holder).setText("");
				break;

			case String:

				if (holder.getClass().equals(ListBox.class)) {
					ListBox tempListBox = (ListBox) holder;
					// if it is not mandatory and not multi-selection, add a disabled option
					// (placeholder)

					// selecting the default value of the list
//					if (field.getDefaultValue() != null) {
//						tempListBox.setSelectedValue(field.getDefaultValue());
//						return;
//					}

					// selecting unique element of the list
//					if(tempListBox.getItemCount()==1) {
//						tempListBox.setSelectedValue(tempListBox.getItemText(0));
//						return;
//					}

					if (!field.isMultiSelection()) {
						listBoxSelectPlaceholder(tempListBox);
					} else {
						// TODO
					}
				} else
					((TextBox) holder).setText("");
				break;

			case File:

				if (holder.getClass().equals(MultipleDilaogUpload.class)
						|| holder.getClass().equals(DialogUpload.class)) {
					DialogUpload dUpload = (DialogUpload) holder;
					dUpload.eraseLastUpload();
				}
				break;
			default:
				break;

			}
		}
	}

	/**
	 * List box select placeholder.
	 *
	 * @param listBox the list box
	 */
	private void listBoxSelectPlaceholder(ListBox listBox) {

		String placeholder = "Select " + field.getFieldName();
		if (listBox.getItemCount() == 0) {
			// create
			listBox.addItem(placeholder);
			listBox.setValue(0, "");
			listBox.getElement().getElementsByTagName("option").getItem(0).setAttribute("disabled", "disabled");
			listBox.setSelectedIndex(0);
		} else {
			// update
			String itemText = listBox.getItemText(0);
			// if the first element is the placeholder, select it
			if (itemText != null && itemText.compareTo(placeholder) == 0) {
				listBox.setValue(0, "");
				listBox.getElement().getElementsByTagName("option").getItem(0).setAttribute("disabled", "disabled");
				listBox.setSelectedIndex(0);
			}
		}
	}

	/**
	 * Freeze this widget (after on create).
	 *
	 * @param bool the bool
	 */
	public void freeze(boolean bool) {

		switch (field.getType()) {

		case Boolean:

			((CheckBox) holder).setEnabled(!bool);
			break;

		case GeoJSON:
		case Text:

			((TextArea) holder).setEnabled(!bool);
			break;

		case Time:

			((DataTimeBox) holder).freeze(!bool);
			break;

		case Time_Interval:

			rangesList.get(0).freeze(!bool);
			break;

		case Times_ListOf:

			for (DataTimeBox el : rangesList)
				el.freeze(bool);

			break;

		case Number:

			((TextBox) holder).setEnabled(!bool);
			break;

		case String:

			if (holder.getClass().equals(ListBox.class))
				((ListBox) holder).setEnabled(!bool);
			else
				((TextBox) holder).setEnabled(!bool);
			break;

		case File:

			if (holder.getClass().equals(MultipleDilaogUpload.class) || holder.getClass().equals(DialogUpload.class)) {
				DialogUpload dUpload = (DialogUpload) holder;
				dUpload.enableUpload(!bool);
			}
			break;
		default:
			break;

		}
	}

	/**
	 * Get the original MetadataFieldWrapper object.
	 *
	 * @return the field
	 */
	public MetadataFieldWrapper getField() {
		return field;
	}

	/**
	 * Removes the error.
	 */
	public void removeError() {

		metafieldControlGroup.setType(ControlGroupType.NONE);

	}

	/**
	 * Show error.
	 */
	public void showError() {

		metafieldControlGroup.setType(ControlGroupType.ERROR);

	}

	/**
	 * Build the range interval.
	 *
	 * @param rangeValues the range values
	 * @param tb          the tb
	 */
	private void setRangeTimeInTimeBox(String rangeValues, DataTimeBox tb) {
		// set time, if present
		if (rangeValues != null && !rangeValues.isEmpty()) {
			if (!rangeValues.contains(DataTimeBox.RANGE_SEPARATOR_START_END))
				rangeValues += "/" + rangeValues;

			String[] dateAndTimeRanges = rangeValues.split(DataTimeBox.RANGE_SEPARATOR_START_END);
			if (dateAndTimeRanges.length > 0) {
				String[] firstRangeDate = dateAndTimeRanges[0].split(" ");
				tb.setStartDate(firstRangeDate[0], firstRangeDate.length > 1 ? firstRangeDate[1] : null);
				if (dateAndTimeRanges.length > 1) {
					String[] secondRangeDate = dateAndTimeRanges[1].split(" ");
					tb.setEndDate(secondRangeDate[0], secondRangeDate.length > 1 ? secondRangeDate[1] : null);
				}
			}
		}
	}

	/**
	 * Given the name and the value of this field, return a tag if it is the case.
	 * 
	 * @return a tag as string
	 */
	public List<String> getTagFromThisField() {

		FieldAsTag asTag = field.getAsTag();
		List<String> generatedTags = new ArrayList<String>();
		if (asTag != null && asTag.isCreate()) {
			List<String> values = getFieldCurrentValue();
			for (String value : values) {
				generatedTags.add(TaggingGroupingValue.getComposedValueTag(field.getFieldName(), value,
						asTag.getSeparator(), asTag.getTaggingValue()));
			}
			return generatedTags;
		}
		return null;
	}

	/**
	 * Given the name and the value of this field, return a group title if it is the
	 * case.
	 * 
	 * @return a tag as string
	 */
	public List<String> getGroupTitleFromThisGroup() {
		FieldAsGroup asGroup = field.getAsGroup();
		List<String> generatedGroups = new ArrayList<String>();
		if (asGroup != null) {
			List<String> values = getFieldCurrentValue();
			for (String value : values) {
				generatedGroups.addAll(TaggingGroupingValue.getComposedValueGroup(field.getFieldName(), value,
						asGroup.getGroupingValue()));
			}
			return generatedGroups;
		}
		return null;
	}

	/**
	 * Check if the group associated (if it exists) should be forced.
	 *
	 * @return true, if is group to force
	 */
	public boolean isGroupToForce() {
		FieldAsGroup asGroup = field.getAsGroup();
		if (asGroup != null) {
			return asGroup.getCreate();
		}
		return false;
	}

	/**
	 * Check if the propagateUp is set.
	 *
	 * @return true, if is propagate up
	 */
	public boolean isPropagateUp() {
		FieldAsGroup asGroup = field.getAsGroup();
		if (asGroup != null) {
			return asGroup.isPropagateUp();
		}
		return false;
	}

}