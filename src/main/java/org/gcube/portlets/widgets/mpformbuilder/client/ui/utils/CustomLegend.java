package org.gcube.portlets.widgets.mpformbuilder.client.ui.utils;

import com.github.gwtbootstrap.client.ui.Legend;
import com.google.gwt.user.client.ui.HTML;


/**
 * The Class CustomLegend.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 8, 2020
 */
public class CustomLegend extends Legend {
	
	public CustomLegend() {
		this.addStyleName("legend-style");
	}

	/**
	 * Instantiates a new custom legend.
	 *
	 * @param text the text
	 * @param addIsRequiredTxt the add is required txt
	 */
	public CustomLegend(String text, boolean addIsRequiredTxt) {
		this();
		setText(text, addIsRequiredTxt);
	}
	
	/**
	 * Instantiates a new custom legend.
	 *
	 * @param text the text
	 * @param addIsRequiredTxt the add is required txt
	 */
	public CustomLegend(String html) {
		this();
		HTML toHMTL = new HTML(html);
		this.add(toHMTL);
	}
	
	
	public void setText(String text, boolean addIsRequiredTxt){
		this.clear();
		
		String htmlToAdd = text;
		if (addIsRequiredTxt)
			htmlToAdd += "<small><span style='color:red;'>&nbsp*</span> is required field</small>";
		
		HTML toHMTL = new HTML(htmlToAdd);

		this.add(toHMTL);
	}

}
