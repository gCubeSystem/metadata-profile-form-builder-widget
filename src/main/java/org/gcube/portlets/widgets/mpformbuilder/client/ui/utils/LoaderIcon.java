/**
 * 
 */
package org.gcube.portlets.widgets.mpformbuilder.client.ui.utils;


import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;

/**
 * The Class LoaderIcon.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 19, 2015
 */
public class LoaderIcon extends HorizontalPanel{
	

	//private Image imgLoading = new Image(Images.ICONS.loading());
	private HTML txtLoading = new HTML("");
	private Icon iconSpinner = new Icon(IconType.SPINNER);
	
	
	/**
	 * Instantiates a new loader icon.
	 *
	 * @param txtHTML the txt html
	 */
	public LoaderIcon(String txtHTML) {
		this();
		setText(txtHTML);
	}
	
	/**
	 * Instantiates a new loader icon.
	 */
	public LoaderIcon() {
		setStyleName("marginTop20");
		add(txtLoading);
		iconSpinner.setSpin(true);
		iconSpinner.getElement().getStyle().setProperty("animation", "spin 1s infinite linear");
		iconSpinner.getElement().getStyle().setMarginLeft(5, Unit.PX);
		add(iconSpinner);
	}
	
	/**
	 * Sets the text.
	 *
	 * @param txtHTML the new text
	 */
	public void setText(String txtHTML){
		txtLoading.setHTML("<span style=\"margin-left:5px; vertical-align:middle;\">"+txtHTML+"</span>");
	}
	
	/**
	 * Show.
	 *
	 * @param bool the bool
	 */
	public void show(boolean bool){
		this.setVisible(bool);
	}

}
