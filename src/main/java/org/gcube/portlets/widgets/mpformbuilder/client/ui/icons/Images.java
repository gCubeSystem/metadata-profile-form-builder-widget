package org.gcube.portlets.widgets.mpformbuilder.client.ui.icons;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images extends ClientBundle {

	public static final Images ICONS = GWT.create(Images.class);

	@Source("file.png")
	ImageResource fileIcon();

	@Source("folder.png")
	ImageResource folderIcon();

	@Source("loading.gif")
	ImageResource loading();

	@Source("completed.gif")
	ImageResource completed();

	@Source("delete.gif")
	ImageResource delete();
	
	@Source("failed.png")
	ImageResource failed();
	
	@Source("abort.png")
	ImageResource aborted();
}
