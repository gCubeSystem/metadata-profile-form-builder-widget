package org.gcube.portlets.widgets.mpformbuilder.client.ui.upload;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class FileUpload extends Composite {

	private static FileUploadUiBinder uiBinder = GWT.create(FileUploadUiBinder.class);

	interface FileUploadUiBinder extends UiBinder<Widget, FileUpload> {
	}
	
	@UiField HTMLPanel fileUploadPanel;

	public FileUpload(String fielName) {
		initWidget(uiBinder.createAndBindUi(this));
		fileUploadPanel.add(new MultipleDilaogUpload(fielName));
		
	}
}
