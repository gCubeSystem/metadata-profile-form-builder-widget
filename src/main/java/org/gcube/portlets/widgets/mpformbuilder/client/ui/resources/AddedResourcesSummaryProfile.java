package org.gcube.portlets.widgets.mpformbuilder.client.ui.resources;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.mpformbuilder.client.events.AddResourceEventProfile;
import org.gcube.portlets.widgets.mpformbuilder.client.events.AddResourceEventProfileHandler;
import org.gcube.portlets.widgets.mpformbuilder.client.events.DeleteResourceProfileEvent;
import org.gcube.portlets.widgets.mpformbuilder.client.events.DeleteResourceProfileEventHandler;
import org.gcube.portlets.widgets.mpformbuilder.shared.catalogue.ResourceElementBeanProfile;

import com.github.gwtbootstrap.client.ui.Accordion;
import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * A summary of the resources added by the user.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class AddedResourcesSummaryProfile extends Composite{

	private static AddedResourcesSummaryUiBinder uiBinder = GWT
			.create(AddedResourcesSummaryUiBinder.class);

	interface AddedResourcesSummaryUiBinder extends
	UiBinder<Widget, AddedResourcesSummaryProfile> {
	}

	// Event bus
	private HandlerManager eventBus;

	// list of added resources (beans)
	List<ResourceElementBeanProfile> addedResources;

	@UiField VerticalPanel addResourcesPanel;

	public AddedResourcesSummaryProfile(HandlerManager eventBus) {
		initWidget(uiBinder.createAndBindUi(this));
		// save bus
		this.eventBus = eventBus;
		// bind on add resource event
		bind();
		// init list
		addedResources = new ArrayList<ResourceElementBeanProfile>();
	}

	/**
	 * Bind on add/delete resource event
	 */
	private void bind() {

		// when a new resource is added
		eventBus.addHandler(AddResourceEventProfile.TYPE, new AddResourceEventProfileHandler() {

			@Override
			public void onAddedResource(AddResourceEventProfile addResourceEvent) {
				GWT.log("Added resource event: "+addResourceEvent);

				// get the resource
				final ResourceElementBeanProfile justAddedResource = addResourceEvent.getResource();

				// Build an accordion to show resource info
				Accordion accordion = new Accordion();
				AccordionGroup accordionGroup = new AccordionGroup();
				accordionGroup.setHeading("- " + justAddedResource.getName());
				accordion.add(accordionGroup);

				// add sub-info such as url and description
				Paragraph pUrl = new Paragraph();
				pUrl.setText("Url : " + justAddedResource.getUrl());
				Paragraph pDescription = new Paragraph();
				pDescription.setText("Description : " + justAddedResource.getDescription());

				// button to delete the resource
				Button deleteButton = new Button();
				deleteButton.setText("Delete");
				deleteButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {

						eventBus.fireEvent(new DeleteResourceProfileEvent(justAddedResource));

					}
				});

				// fill accordion
				accordionGroup.add(pUrl);
				accordionGroup.add(pDescription);
				accordionGroup.add(deleteButton);

				// add to the list
				addedResources.add(justAddedResource);

				// add to the panel
				addResourcesPanel.add(accordion);
			}
		});

		// when the user wants to delete a resource
		eventBus.addHandler(DeleteResourceProfileEvent.TYPE, new DeleteResourceProfileEventHandler() {

			@Override
			public void onDeletedResource(DeleteResourceProfileEvent deleteResourceEvent) {

				// to delete
				ResourceElementBeanProfile toDelete = deleteResourceEvent.getResource();
				Window.alert("DELETE RESOURCE MUST BE IMPLEMENTED");
				/*
				 for(int i = 0; i < addedResources.size(); i++){

					if(addedResources.get(i).equals(toDelete)){
						
						// get the associated widget and remove it
						final Widget widget = addResourcesPanel.getWidget(i);

						// remote call to remove it from the dataset
						MetadataProfileFormBuilderServiceAsync.Util.getInstance().deleteResourceFromDataset(toDelete, new AsyncCallback<Boolean>() {

							@Override
							public void onSuccess(Boolean result) {
								
								if(result)
									widget.removeFromParent();
							}

							@Override
							public void onFailure(Throwable caught) {

							}
						});
						
						break;
					}
				}
				*/
				// remove from the list
				addedResources.remove(toDelete);
			}
		});
	}
	
	public HandlerManager getEventBus() {
		return eventBus;
	}
}
