package org.gcube.portlets.widgets.mpformbuilder.client;

import java.util.List;

import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 22, 2020
 */
@RemoteServiceRelativePath("mpformbuilderservice")
public interface MetadataProfileFormBuilderService extends RemoteService {

	/**
	 * Gets the profiles in the scope.
	 *
	 * @param scope the scope
	 * @param genericResourceSecondaryType the generic resource secondary type
	 * @return the profiles in the scope
	 * @throws Exception the exception
	 */
	List<MetaDataProfileBean> getProfilesInTheScope(String scope, String genericResourceSecondaryType) throws Exception;

	/**
	 * Gets the profile for metadata.
	 *
	 * @param metadata the metadata
	 * @return the profile for metadata
	 * @throws Exception the exception
	 */
	MetaDataProfileBean getProfileForMetadata(String metadata) throws Exception;

	/**
	 * Gets the upload status.
	 *
	 * @param clientUploadKey the client upload key
	 * @return the upload status
	 * @throws Exception the exception
	 */
	FileUploadingState getUploadStatus(String clientUploadKey) throws Exception;

	/**
	 * Purge files uploaded.
	 *
	 * @return number of files deleted. Null otherwise.
	 * @throws Exception the exception
	 */
	Integer purgeFilesUploaded() throws Exception;

	/**
	 * Gets the profiles in the scope.
	 *
	 * @param scope the scope
	 * @param genericResourceSecondaryType the generic resource secondary type
	 * @param resourceName the resource name
	 * @return the profiles in the scope
	 * @throws Exception the exception
	 */
	List<MetaDataProfileBean> getProfilesInTheScopeForName(String scope,String genericResourceSecondaryType, String resourceName) throws Exception;
}
