package org.gcube.portlets.widgets.mpformbuilder.client.events;

import org.gcube.portlets.widgets.mpformbuilder.shared.catalogue.ResourceElementBeanProfile;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Added resource event
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class AddResourceEventProfile extends GwtEvent<AddResourceEventProfileHandler> {
	public static Type<AddResourceEventProfileHandler> TYPE = new Type<AddResourceEventProfileHandler>();

	private ResourceElementBeanProfile resource;

	public AddResourceEventProfile(ResourceElementBeanProfile resource) {
		this.resource = resource;
	}

	public ResourceElementBeanProfile getResource() {
		return resource;
	}

	@Override
	public Type<AddResourceEventProfileHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AddResourceEventProfileHandler handler) {
		handler.onAddedResource(this);
	}
}
