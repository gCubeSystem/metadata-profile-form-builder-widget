package org.gcube.portlets.widgets.mpformbuilder.client.ui.resources;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;



/**
 * The Class AddResourceToDataset.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 1, 2020
 */
public class AddResourceToDataset extends Composite{

	private static AddResourceToDatasetUiBinder uiBinder = GWT
			.create(AddResourceToDatasetUiBinder.class);

	/**
	 * The Interface AddResourceToDatasetUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Oct 1, 2020
	 */
	interface AddResourceToDatasetUiBinder extends
	UiBinder<Widget, AddResourceToDataset> {
	}

	// bus to alert the dataset form about this new resource
	private HandlerManager eventBus;

	// the dataset id
	private String datasetId;

	// the dataset organization
	private String datasetOrg;

	@UiField TextBox resourceUrlTextBox;
	@UiField TextBox resourceNameTextBox;
	@UiField TextArea resourceDescriptionTextArea;
	@UiField Button addResourceButton;
	@UiField AlertBlock infoBlock;
	@UiField Button goToDatasetButton;

	/**
	 * Instantiates a new adds the resource to dataset.
	 *
	 * @param eventBus the event bus
	 * @param datasetId the dataset id
	 * @param datasetOrg the dataset org
	 * @param owner the owner
	 * @param datasetUrl the dataset url
	 */
	public AddResourceToDataset(HandlerManager eventBus, String datasetId, String datasetOrg, String owner, final String datasetUrl) {
		initWidget(uiBinder.createAndBindUi(this));

		// save bus
		this.eventBus = eventBus;

		// save dataset id (it is needed when we will add resources)
		this.datasetId = datasetId;

		this.datasetOrg = datasetOrg;
		goToDatasetButton.setText(		
				(datasetUrl.length() > 100 ? 
						datasetUrl.substring(0, 100) + "..." : datasetUrl)
				);
		//		goToDatasetButton.setHref(datasetUrl);
		goToDatasetButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Window.open(datasetUrl, "_blank", "");
			}
		});
	}

	/**
	 * On add button click.
	 *
	 * @param e the e
	 */
	@UiHandler("addResourceButton")
	void onAddButtonClick(ClickEvent e){

		infoBlock.setVisible(false);

		// validation
		if(resourceUrlTextBox.getText().isEmpty() || resourceNameTextBox.getText().isEmpty()){

			showAlert("Url and name fields cannot be empty", AlertType.ERROR);
			return;

		}

		// collect data and build up the bean
		/*//TODO MUST BE DONE IN ANOTHER POINT
		 * 
		 * final ResourceElementBeanProfile resource = 
				new ResourceElementBeanProfile(
						resourceNameTextBox.getText(),
						true, 
						false, 
						null, 
						null, 
						null, 
						null, 
						null, 
						resourceUrlTextBox.getText(), 
						resourceDescriptionTextArea.getText(), 
						datasetOrg);

		// disable add button
		addResourceButton.setEnabled(false);

		// try to create
		ckanServices.addResourceToDataset(resource, datasetId, new AsyncCallback<ResourceElementBeanProfile>() {

			@Override
			public void onSuccess(ResourceElementBeanProfile result) {

				if(result != null){
					showAlert("Resource created correctly", AlertType.SUCCESS);
					eventBus.fireEvent(new AddResourceEventProfile(result));

					// remove data
					resourceUrlTextBox.setText("");
					resourceNameTextBox.setText("");
					resourceDescriptionTextArea.setText("");

				}
				else
					showAlert("Unable to add this resource. Check that the url is correct", AlertType.ERROR);

			}

			@Override
			public void onFailure(Throwable caught) {

				showAlert("Unable to add this resource, sorry. Error is: " + caught.getMessage(), AlertType.ERROR);

			}
		});*/

	}

	/**
	 * Show error/success after resource creation attempt.
	 *
	 * @param text the text
	 * @param type the type
	 */
	protected void showAlert(String text, AlertType type) {

		infoBlock.setText(text);
		infoBlock.setType(type);
		infoBlock.setVisible(true);
		addResourceButton.setEnabled(true);

		// hide after some seconds
		Timer t = new Timer() {

			@Override
			public void run() {

				infoBlock.setVisible(false);

			}
		};

		t.schedule(4000);
	}
}
