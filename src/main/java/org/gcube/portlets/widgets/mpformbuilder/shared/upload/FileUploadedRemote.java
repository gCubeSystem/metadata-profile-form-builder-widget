package org.gcube.portlets.widgets.mpformbuilder.shared.upload;

/**
 * The Class FileUploadedRemote.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 1, 2023
 */
public class FileUploadedRemote extends FileUploaded {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2991541932545131966L;
	private String url;
	private String mimeType;

	/**
	 * Instantiates a new file uploaded remote.
	 */
	public FileUploadedRemote() {

	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 *
	 * @param mimeType the new mime type
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileUploadedRemote [url=");
		builder.append(url);
		builder.append(", mimeType=");
		builder.append(mimeType);
		builder.append("]");
		return builder.toString();
	}

}
