package org.gcube.portlets.widgets.mpformbuilder.shared.metadata;

public interface ErasableField {

	public boolean isErasableField();
}
