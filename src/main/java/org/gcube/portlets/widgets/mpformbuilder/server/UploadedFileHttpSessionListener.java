package org.gcube.portlets.widgets.mpformbuilder.server;

import java.util.List;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public class UploadedFileHttpSessionListener implements HttpSessionListener {

	private static final Logger LOG = LoggerFactory.getLogger(UploadedFileHttpSessionListener.class);
	
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		LOG.trace("sessionDestroyed called");
		
		try {
			HttpSession httpSession = event.getSession();
			if(httpSession!=null) {
				LOG.trace("Sesson id is: "+httpSession.getId());
				List<FileUploaded> listFileUploaded = (List<FileUploaded>) httpSession.getAttribute(ConstantsMPFormBuilder.FILE_UPLOADED_SESSION_ATTR);
				if(listFileUploaded!=null) {
					LOG.info("found file uploded in session, removing it");
					for (FileUploaded fileUploaded : listFileUploaded) {
						FileUtil.deleteFile(fileUploaded.getTempSystemPath());
					}
				}
			}
		}catch (Exception e) {
			LOG.warn("Error tryng to delete uploaded file", e);
		}
		
	}

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		LOG.trace("sessionCreated called. Session id is: "+arg0.getSession().getId());
	}
}
