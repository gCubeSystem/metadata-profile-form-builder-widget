package org.gcube.portlets.widgets.mpformbuilder.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface AddResourceEventProfileHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 12, 2024
 */
public interface AddResourceEventProfileHandler extends EventHandler {
	
	/**
	 * On added resource.
	 *
	 * @param addResourceEvent the add resource event
	 */
	void onAddedResource(AddResourceEventProfile addResourceEvent);
}
