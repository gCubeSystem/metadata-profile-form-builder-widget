package org.gcube.portlets.widgets.mpformbuilder.shared;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;

/**
 * The Class GenericDatasetBean.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 19, 2020
 */
public class GenericDatasetBean<T extends MetaDataProfileBean, F extends FileUploaded> implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5215392381589702647L;

	/** The metadata profile list. */
	private List<T> metadataProfileList;

	/** The form data entry fields. */
	private LinkedHashMap<String, List<String>> formDataEntryFields;

	/** The files uploaded. */
	private List<F> filesUploaded;

	/**
	 * Instantiates a new generic dataset bean.
	 */
	public GenericDatasetBean() {
	}

	/**
	 * Instantiates a new generic dataset bean.
	 *
	 * @param metadataProfileList the metadata profile list
	 * @param formDataEntryFields the form data entry fields
	 * @param filesUploaded       the files uploaded
	 */
	public GenericDatasetBean(List<T> metadataProfileList, LinkedHashMap<String, List<String>> formDataEntryFields,
			List<F> filesUploaded) {
		super();
		this.metadataProfileList = metadataProfileList;
		this.formDataEntryFields = formDataEntryFields;
		this.filesUploaded = filesUploaded;
	}

	/**
	 * Gets the metadata profile list.
	 *
	 * @return the metadata profile list
	 */
	public List<T> getMetadataProfileList() {
		return metadataProfileList;
	}

	/**
	 * Sets the metadata profile list.
	 *
	 * @param metadataProfileList the new metadata profile list
	 */
	public void setMetadataProfileList(List<T> metadataProfileList) {
		this.metadataProfileList = metadataProfileList;
	}

	/**
	 * Gets the form data entry fields.
	 *
	 * @return the form data entry fields
	 */
	public LinkedHashMap<String, List<String>> getFormDataEntryFields() {
		return formDataEntryFields;
	}

	/**
	 * Sets the form data entry fields.
	 *
	 * @param formDataEntryFields the form data entry fields
	 */
	public void setFormDataEntryFields(LinkedHashMap<String, List<String>> formDataEntryFields) {
		this.formDataEntryFields = formDataEntryFields;
	}

	/**
	 * Gets the files uploaded.
	 *
	 * @return the files uploaded
	 */
	public List<F> getFilesUploaded() {
		return filesUploaded;
	}

	/**
	 * Sets the files uploaded.
	 *
	 * @param filesUploaded the new files uploaded
	 */
	public void setFilesUploaded(List<F> filesUploaded) {
		this.filesUploaded = filesUploaded;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "GenericDatasetBean [metadataProfileList=" + metadataProfileList + ", formDataEntryFields="
				+ formDataEntryFields + ", filesUploaded=" + filesUploaded + "]";
	}

}