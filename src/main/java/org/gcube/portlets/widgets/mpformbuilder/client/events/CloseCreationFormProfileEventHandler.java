package org.gcube.portlets.widgets.mpformbuilder.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface CloseCreationFormProfileEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 12, 2024
 */
public interface CloseCreationFormProfileEventHandler extends EventHandler {

	/**
	 * On close.
	 *
	 * @param event the event
	 */
	void onClose(CloseCreationFormProfileEvent event);
}
