/**
 * 
 */
package org.gcube.portlets.widgets.mpformbuilder.client.form.generic;

import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericFormEvents.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 9, 2020
 */
public class GenericFormEvents {

	
	/**
	 * The listener interface for receiving genericFormEvents events.
	 * The class that is interested in processing a genericFormEvents
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addGenericFormEventsListener<code> method. When
	 * the genericFormEvents event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see GenericFormEventsEvent
	 */
	public interface GenericFormEventsListener {

		/**
		 * On valid form.
		 *
		 * @param genericDatasetBean the generic dataset bean
		 */
		void onFormDataValid(GenericDatasetBean genericDatasetBean);
		
		/**
		 * On form data edit.
		 */
		void onFormDataEdit();


		/**
		 * On form aborted.
		 */
		void onFormAborted();
		
		/**
		 * On validation error.
		 *
		 * @param throwable the throwable
		 * @param errorMsg the error msg
		 */
		void onValidationError(Throwable throwable, String errorMsg);
	}
	
	
	/**
	 * The Interface HasGenericFormListenerRegistration.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Oct 9, 2020
	 */
	public interface HasGenericFormListenerRegistration {

	
		
		/**
		 * Adds the listener.
		 *
		 * @param listener the listener
		 */
		public void addListener(GenericFormEventsListener listener);


		/**
		 * Removes the listener.
		 *
		 * @param listener the listener
		 */
		public void removeListener(GenericFormEventsListener listener);

	}

}