package org.gcube.portlets.widgets.mpformbuilder.server;

import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONFileUploaded {

	@JsonProperty(value = ConstantsMPFormBuilder.UPL_CLIENT_KEY)
	protected String clienKey;
	@JsonProperty(value = ConstantsMPFormBuilder.UPL_FILENAME)
	protected String fileName;

	// THIS IS THE FIELD LABEL USED IN THE WEB-FORM
	// AND DECLARED IN THE FIELDNAME OF THE GCUBE METADATA PROFILE
	@JsonProperty(value = ConstantsMPFormBuilder.UPL_FIELD_NAME_FILEPATH)
	protected String fieldNameFilePath;

	public JSONFileUploaded() {
		super();
	}

	public String getClienKey() {
		return clienKey;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFieldNameFilePath() {
		return fieldNameFilePath;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JSONFileUploaded [clienKey=");
		builder.append(clienKey);
		builder.append(", fileName=");
		builder.append(fileName);
		builder.append(", fieldNameFilePath=");
		builder.append(fieldNameFilePath);
		builder.append("]");
		return builder.toString();
	}

}
