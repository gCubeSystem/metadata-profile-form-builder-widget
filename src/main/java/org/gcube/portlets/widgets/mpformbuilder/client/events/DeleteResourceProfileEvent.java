package org.gcube.portlets.widgets.mpformbuilder.client.events;

import org.gcube.portlets.widgets.mpformbuilder.shared.catalogue.ResourceElementBeanProfile;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Deleted resource event.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class DeleteResourceProfileEvent extends GwtEvent<DeleteResourceProfileEventHandler> {
	public static Type<DeleteResourceProfileEventHandler> TYPE = new Type<DeleteResourceProfileEventHandler>();

	private ResourceElementBeanProfile resource;
	
	public DeleteResourceProfileEvent(ResourceElementBeanProfile resource) {
		this.resource = resource;
	}

	public ResourceElementBeanProfile getResource() {
		return resource;
	}
	
	@Override
	public Type<DeleteResourceProfileEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DeleteResourceProfileEventHandler handler) {
		handler.onDeletedResource(this);
	}

}
