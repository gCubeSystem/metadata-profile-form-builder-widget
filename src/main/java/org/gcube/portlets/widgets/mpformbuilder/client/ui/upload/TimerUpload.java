/**
 *
 */
package org.gcube.portlets.widgets.mpformbuilder.client.ui.upload;

import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderServiceAsync;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState.UPLOAD_STATUS;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;


// TODO: Auto-generated Javadoc
/**
 * The Class TimerUpload.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it at ISTI-CNR
 * 
 * Oct 6, 2020
 */
public class TimerUpload extends Timer{

	/** The workspace uploader. */
	private FileUploadingState initWorkspaceUploader;
	
	/** The last workspace uploader. */
	private FileUploadingState lastWorkspaceUploader;
	
	/** The instance. */
	private TimerUpload INSTANCE = this;
	
	/** The upv. */
	private UploaderProgressView upv;

	/**
	 * Instantiates a new timer upload.
	 *
	 * @param workspaceUploader the workspace uploader
	 * @param upv the upv
	 */
	public TimerUpload(FileUploadingState workspaceUploader, UploaderProgressView upv) {
		this.initWorkspaceUploader = workspaceUploader;
		this.upv = upv;
		
	}

	/**
	 * Run.
	 */
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.Timer#run()
	 */
	@Override
	public void run() {
		
		ConstantsMPFormBuilder.jslog("Timer run called");
		
		final SingleUploadMonitor monitor = SingleUploadMonitor.getInstance();
		
		ConstantsMPFormBuilder.jslog("Calling getUpload Status...");
		
		MetadataProfileFormBuilderServiceAsync.Util.getInstance().getUploadStatus(initWorkspaceUploader.getClientUploadKey(), new AsyncCallback<FileUploadingState>() {

			@Override
			public void onSuccess(final FileUploadingState result) {
				ConstantsMPFormBuilder.jslog("getUploadStatus returned: "+result);
				try{
					synchronized(this){
						lastWorkspaceUploader = result;
	//					Timer tmn = queue.get(workspaceUploader.getClientUploadKey());
						if(INSTANCE!=null && INSTANCE.isRunning()){
							if(result==null || result.getUploadStatus()==null){
								GWT.log("Upload or status its status is null for: "+initWorkspaceUploader.getClientUploadKey());
								return;
							}
							if(result.getUploadStatus().equals(UPLOAD_STATUS.COMPLETED)){
								GWT.log("Upload Completed "+result.getFile().getFileName() +" name: "+result.getFile().getFileName());
	//							queue.remove(workspaceUploader.getClientUploadKey());
	//							monitor.deleteUploaderByClientKey(workspaceUploader.getClientUploadKey());

								ConstantsMPFormBuilder.jslog("Upload Completed for file: "+result.getFile().getFileName());

								Timer timer = new Timer() {

									@Override
									public void run() {
										ConstantsMPFormBuilder.jslog("Upload Completed "+result.getFile().getFileName());
										monitor.notifyUploadCompleted(result.getFile().getFileName(), result.getFile().getFileName());
									}
								};
								//THIS IS A DELAY TO WAIT REPOSITORY SYNCHRONIZATION
								timer.schedule(1000);

								cancel();
							}else if(result.getUploadStatus().equals(UPLOAD_STATUS.FAILED)){
								//GWT.log("Upload Failed "+result.getFile().getItemId() +" name: "+result.getFile().getFileName());
								GWT.log("Upload Failed Upload Key: "+result.getClientUploadKey() +" status description: "+result.getStatusDescription());
	//							monitor.deleteUploaderByClientKey(workspaceUploader.getClientUploadKey());
	//							queue.remove(workspaceUploader.getClientUploadKey());
								monitor.notifyUploadError(result.getFile().getFileName(), null, result.getStatusDescription(), null);
	//								new DialogResult(null, "Upload Failed!!", result.getStatusDescription()).center();

								cancel();
							}else if(result.getUploadStatus().equals(UPLOAD_STATUS.ABORTED)){
	//							monitor.deleteUploaderByClientKey(workspaceUploader.getClientUploadKey());
								GWT.log("Upload Aborted Upload Key: "+result.getClientUploadKey() +" status description: "+result.getStatusDescription());
								cancel();
								monitor.notifyUploadAborted(result.getFile().getFileName(), null);
							}
							
							//is running
							upv.update(result);;
						}else{
	//						monitor.deleteUploaderByClientKey(workspaceUploader.getClientUploadKey());
							ConstantsMPFormBuilder.jslog("Timer is null or already closed or completed");
							cancel();
							monitor.removeTimer(initWorkspaceUploader.getClientUploadKey());
						}
					}
				}catch(Exception e){
					GWT.log("getUploadStatus exception "+e.getMessage());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				ConstantsMPFormBuilder.jslog("onFailure: "+caught.getMessage());
				cancel();
//				monitor.deleteUploaderByClientKey(workspaceUploader.getClientUploadKey());
				monitor.notifyUploadError(initWorkspaceUploader.getFile().getFileName(), null, null, caught);
//				removeTimer(workspaceUploader.getClientUploadKey());
			}
		});

	}

	/**
	 * Cancel.
	 */
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.Timer#cancel()
	 */
 	@Override
	public void cancel() {
		ConstantsMPFormBuilder.jslog("Cancelling timer for "+lastWorkspaceUploader);
		super.cancel();
	}

	public FileUploadingState getInitWorkspaceUploader() {
		return initWorkspaceUploader;
	}

	public FileUploadingState getLastWorkspaceUploader() {
		return lastWorkspaceUploader;
	}
	
	protected void resetLastWorkspaceUploader() {
		lastWorkspaceUploader = null;
	}

	
}
