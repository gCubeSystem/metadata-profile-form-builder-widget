/**
 *
 */
package org.gcube.portlets.widgets.mpformbuilder.shared.upload;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The Class FileUploaded.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 8, 2020
 */
public class FileUploaded implements Serializable, IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6666006742990360875L;

	/** The file name. */
	private String fileName;

	/** The item id. */
	private String tempSystemPath;

	private String createdByUser;

	private FilePath filePath;

	/**
	 * Instantiates a new workspace upload file.
	 */
	public FileUploaded() {
	}

	/**
	 * Instantiates a new file uploaded.
	 *
	 * @param fileName       the file name
	 * @param tempSystemPath the temp system path
	 * @param createdByUser  the created by user
	 * @param filePath       the file path
	 */
	public FileUploaded(String fileName, String tempSystemPath, String createdByUser, FilePath filePath) {
		super();
		this.fileName = fileName;
		this.tempSystemPath = tempSystemPath;
		this.createdByUser = createdByUser;
		this.filePath = filePath;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the temp system path.
	 *
	 * @return the temp system path
	 */
	public String getTempSystemPath() {
		return tempSystemPath;
	}

	/**
	 * Sets the temp system path.
	 *
	 * @param tempSystemPath the new temp system path
	 */
	public void setTempSystemPath(String tempSystemPath) {
		this.tempSystemPath = tempSystemPath;
	}

	/**
	 * Gets the created by user.
	 *
	 * @return the created by user
	 */
	public String getCreatedByUser() {
		return createdByUser;
	}

	/**
	 * Sets the created by user.
	 *
	 * @param createdByUser the new created by user
	 */
	public void setCreatedByUser(String createdByUser) {
		this.createdByUser = createdByUser;
	}

	/**
	 * Gets the file paths.
	 *
	 * @return the file paths
	 */
	public FilePath getFilePath() {
		return filePath;
	}

	/**
	 * Sets the file path.
	 *
	 * @param filePath the new file path
	 */
	public void setFilePath(FilePath filePath) {
		this.filePath = filePath;
	}



}
