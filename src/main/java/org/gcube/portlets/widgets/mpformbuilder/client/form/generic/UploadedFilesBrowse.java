package org.gcube.portlets.widgets.mpformbuilder.client.form.generic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadedRemote;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class UploadedFilesBrowse<T extends FileUploaded> extends Composite {

	private static UploadedFilesBrowseUiBinder uiBinder = GWT.create(UploadedFilesBrowseUiBinder.class);

	interface UploadedFilesBrowseUiBinder extends UiBinder<Widget, UploadedFilesBrowse> {
	}

	private List<T> listFileUploaded;

	@UiField
	HTMLPanel uploadFileContainer;

	private FlexTable table = new FlexTable();

	private List<Button> listButtonRemove = new ArrayList<Button>();

	private HashMap<Integer, T> mapPositionalFU = new HashMap<Integer, T>();

	public UploadedFilesBrowse(List<T> fileUploaded) {
		initWidget(uiBinder.createAndBindUi(this));
		this.listFileUploaded = fileUploaded;
		// Filling map of files uploaded
		int i = 0;
		for (T file : listFileUploaded) {
			mapPositionalFU.put(i, file);
			i++;
		}

		showFileBrowseInteraction();

	}

	private void showFileBrowseInteraction() {
		uploadFileContainer.clear();
		// pathIndex = pathContentIndex;
		// GWT.log("showing pathContentIndex: "+pathContentIndex);
		GWT.log("showing files: " + listFileUploaded);
		table.clear();
		if (listFileUploaded.size() > 0) {
			table.addStyleName("table-current-content");
			table.setHTML(0, 0, "<span style='color:rgb(155, 80, 78); font-weight:bold;'>Current content:</span>");
			table.setHTML(1, 0, "<span style='color:rgb(155, 80, 78);'>Filename</span>");
			table.setHTML(1, 1, "<span style='color:rgb(155, 80, 78);'>MimeType<span>");
			table.setHTML(1, 2, "<span style='color:rgb(155, 80, 78);'>Field Label</span>");
			table.setHTML(1, 3, "<span style='color:rgb(155, 80, 78);'>Link</span>");

			int i = 2;
			int index = 0;

			for (final FileUploaded file : listFileUploaded) {
				final int filePointer = index;
				int columnIndex = 0;
				table.setHTML(i, columnIndex, file.getFileName());
				if (file instanceof FileUploadedRemote) {
					FileUploadedRemote fur = (FileUploadedRemote) file;
					table.setHTML(i, ++columnIndex, fur.getMimeType());
					String fieldLabel = file.getFilePath().getFormFieldLabel();
					table.setHTML(i, ++columnIndex, fieldLabel);
					String link = "<a target=\"_blank\" href=" + fur.getUrl() + ">View</a>";
					table.setHTML(i, ++columnIndex, link);
				}

				final int rowIndexToRem = i;
				Button buttonRemoveFile = new Button();
				buttonRemoveFile.setIcon(IconType.TRASH);
				buttonRemoveFile.setTitle("Remove this file");
				buttonRemoveFile.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						mapPositionalFU.remove(filePointer);
						table.getRowFormatter().getElement(rowIndexToRem).setAttribute("hidden", "hidden");
					}
				});
				listButtonRemove.add(buttonRemoveFile);
				table.setWidget(i, ++columnIndex, buttonRemoveFile);
				i++;
				index++;
			}

			uploadFileContainer.add(table);
		}

	}

	public void enableManageOfContent(boolean bool) {

		for (Button button : listButtonRemove) {
			button.setEnabled(bool);
		}

		double opacity = bool ? 1 : 0.8;

		uploadFileContainer.getElement().getStyle().setOpacity(opacity);

	}

	public List<T> getListRemainingFileUploaded() {

		Collection<T> collFileUplaoded = mapPositionalFU.values();

		if (collFileUplaoded != null)
			return new ArrayList<T>(collFileUplaoded);

		return null;
	}

}
