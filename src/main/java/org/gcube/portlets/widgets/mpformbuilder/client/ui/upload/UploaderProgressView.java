/**
 *
 */
package org.gcube.portlets.widgets.mpformbuilder.client.ui.upload;

import org.gcube.portlets.widgets.mpformbuilder.client.ui.icons.Images;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.LoaderIcon;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.StringUtil;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * The Class UploaderProgressView.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Oct 12, 2015
 */
public class UploaderProgressView {

	private VerticalPanel vp = new VerticalPanel();
	private static final int MAX_CHARS = 50;
	private HorizontalPanel hp = new HorizontalPanel();
	private HorizontalPanel hpBar = new HorizontalPanel();
	private Alert alert = new Alert();
	private ProgressBar bar = new ProgressBar();
	private HandlerManager eventBus;
	private Image cancelImg = new Image(Images.ICONS.delete());
	private FileUploadingState lastUploaderLoaded;

	/**
	 * Instantiates a new uploader progress view.
	 *
	 * @param uploader the uploader
	 * @param fileName the file name
	 * @param eventBus the event bus
	 */
	public UploaderProgressView(FileUploadingState uploader, final String fileName, HandlerManager eventBus) {
		this.eventBus = eventBus;
		cancelImg.setTitle("Cancel upload");
		cancelImg.addStyleName("cancel-upload");
		hpBar.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		hp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

//		String text = "<div><img src='"+Images.ICONS.loading().getURL()+"'>";
//		String msg = StringUtil.ellipsize("Uploading "+fileName, MAX_CHARS, 0);
//		text+="<span style='margin-left:5px; vertical-align: top;'>"+msg+"</span>";
//		text+="</div>";
//
//		html.setHTML(text);
//		html.setTitle("Uploading "+fileName);
		
		alert.setAnimation(true);
		alert.setClose(false);
		alert.setType(AlertType.DEFAULT);
		alert.add(new LoaderIcon("Uploading "+fileName));
		hp.add(alert);

		bar.update(0);
		setVisibleBar(false);
		setVisibleCancel(false);
		vp.add(hp);

		//TODO DISPLAY PROGRESS BAR...
		//hpBar.add(cancelImg);
		//hpBar.add(bar);
		//vp.add(hpBar);
	}

	/**
	 * Sets the visible cancel.
	 *
	 * @param b the new visible cancel
	 */
	private void setVisibleCancel(boolean b) {
		cancelImg.setVisible(b);
	}

	/**
	 * Sets the visible bar.
	 *
	 * @param bool the new visible bar
	 */
	public void setVisibleBar(boolean bool){
		bar.setVisible(bool);
	}

	/**
	 * Update.
	 *
	 * @param uploader the uploader
	 */
	public void update(FileUploadingState uploader){
		String text;
		try{
			this.lastUploaderLoaded = uploader;
			
			HTML html;
			switch(uploader.getUploadStatus()){
			case COMPLETED:
				setVisibleBar(true);
				text = "<div><img src='"+Images.ICONS.completed().getURL()+"'>";
				String msgClt = uploader.getFile().getFileName()+" uploaded successfully!";

				text+="<span style='margin-left:5px; vertical-align: top;'>"+msgClt+"</span></div>";
	//			GWT.log(text);
				hp.clear();
				alert = new Alert();
				alert.setClose(false);
				alert.setType(AlertType.SUCCESS);
				html = new HTML(text);
				html.setTitle(uploader.getStatusDescription());
				alert.add(html);
				hp.add(alert);
				bar.update(uploader.getUploadProgress().getLastEvent().getReadPercentage());
//				try{
//					hpBar.clear();
//				}catch (Exception e) {}
				break;
			case FAILED:
				setVisibleBar(true);
				text = "<div><img src='"+Images.ICONS.failed().getURL()+"'>";
				text+="<span style='margin-left:5px; vertical-align: top;'>"+StringUtil.ellipsize(uploader.getStatusDescription(), MAX_CHARS, 0)+"</span></div>";
	//			GWT.log(text);
				hp.clear();
				alert = new Alert();
				alert.setClose(false);
				alert.setType(AlertType.ERROR);
				html = new HTML(text);
				html.setTitle(uploader.getStatusDescription());
				alert.add(html);
				hp.add(alert);
	//			bar.update(uploader.getUploadProgress().getLastEvent().getReadPercentage());
				try{
					hpBar.clear();
				}catch (Exception e) {}
				break;
			case ABORTED:
				setVisibleBar(true);
				text = "<div><img src='"+Images.ICONS.aborted().getURL()+"'>";
				text+="<span style='margin-left:5px; vertical-align: top;'>"+uploader.getStatusDescription()+"</span></div>";
	//			GWT.log(text);
				hp.clear();
				alert = new Alert();
				alert.setClose(false);
				alert.setType(AlertType.ERROR);
				html = new HTML(text);
				html.setTitle(uploader.getStatusDescription());
				alert.add(html);
	//			bar.update(uploader.getUploadProgress().getLastEvent().getReadPercentage());
				try{
					hpBar.clear();
				}catch (Exception e) {}
				break;
			case IN_PROGRESS:
				setVisibleBar(true);
	//			text = "<div><img src='"+WorkspaceUploaderResources.getImageUpload().getUrl()+"'>";
				text = "<div><img src='"+Images.ICONS.loading().getURL()+"'>";

				String msg = "";
				if(uploader.getUploadProgress().getLastEvent().getReadPercentage()<100){
					msg = uploader.getFile().getFileName();
				}else{ //is 100%
					String message = "Finalising the upload of "+ uploader.getFile().getFileName();
					msg = message;
				}

				text+="<span style='margin-left:5px; vertical-align: top;'>"+msg+"</span>";
				text+="</div>";
	//			GWT.log(text);
				hp.clear();
				alert = new Alert();
				alert.setClose(false);
				alert.setType(AlertType.ERROR);
				html = new HTML(text);
				html.setTitle(uploader.getStatusDescription());
				alert.add(html);

				//TODO CANCEL OPERATION MUST BE ENHANCED IN ORDER TO CANCELL ALL UPLOADS
				/*
				if(uploader.getUploadProgress().getLastEvent().getReadPercentage()!=100 && !cancel){
					cancel = true;
					handleCancelUpload(uploader);
					setVisibleCancel(true);
				}else if(uploader.getUploadProgress().getLastEvent().getReadPercentage()==100 && cancel){
					try{
						setVisibleCancel(false);
	//					hp.remove(cancelImg);
					}catch (Exception e) {}
				}*/

				bar.update(uploader.getUploadProgress().getLastEvent().getReadPercentage());
				break;
			case WAIT:
				setVisibleBar(false);
				text = "<div><img src='"+Images.ICONS.loading().getURL()+"'>";
				String descr = "";
				if(uploader.getFile()!=null && uploader.getFile().getFileName()!=null)
					descr = uploader.getFile().getFileName();
				else
					descr = uploader.getStatusDescription();

				text+="<span style='margin-left:5px; vertical-align: top;'>"+descr+"</span></div>";
				text+="</div>";
	//			GWT.log(text);
				alert.clear();
				alert.setClose(false);
				alert.setType(AlertType.ERROR);
				html = new HTML(text);
				alert.add(html);

				if(uploader.getUploadProgress()!=null && uploader.getUploadProgress().getLastEvent()!=null)
					bar.update(uploader.getUploadProgress().getLastEvent().getReadPercentage());
				else
					bar.update(0);

				break;
			default:
				break;
			}
		}catch(Exception e) {
			GWT.log("error during update");
		}
	}
	
	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public VerticalPanel getPanel() {
		return vp;
	}
	
	
	/**
	 * Gets the last uploader loaded.
	 *
	 * @return the last uploader loaded
	 */
	public FileUploadingState getLastUploaderLoaded() {
		return lastUploaderLoaded;
	}


//	/**
//	 * Handle cancel upload.
//	 *
//	 * @param uploader the uploader
//	 * @return the image
//	 */
//	private Image handleCancelUpload(final WorkspaceUploaderItem uploader){
//
//		cancelImg.addClickHandler(new ClickHandler() {
//
//			@Override
//			public void onClick(ClickEvent event) {
//				GWT.log("Click cancel");
//				String fileName = uploader.getFile()!=null? uploader.getFile().getFileName(): "";
//				if(Window.confirm("Confirm cancel uploading "+fileName+"?")){
//					hp.clear();
//					HTML html = new HTML();
//					hp.add(html);
//					String text = "<div><img src='"+Images.ICONS.delete().getURL()+"'>";
//					String msg = StringUtil.ellipsize("Aborting upload: "+fileName, MAX_CHARS, 0);
//					text+="<span style='margin-left:5px; vertical-align: top;'>"+msg+"</span></div>";
//					html.setHTML(text);
//					eventBus.fireEvent(new CancelUploadEvent(uploader, UploaderProgressView.this, fileName));
//				}
//			}
//		});
//		return cancelImg;
//	}



}
