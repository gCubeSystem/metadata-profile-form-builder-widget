package org.gcube.portlets.widgets.mpformbuilder.client.ui.upload;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portal.clientcontext.client.GCubeClientContext;
import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.WorkspaceUploadNotification.HasWorskpaceUploadNotificationListener;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.WorkspaceUploadNotification.WorskpaceUploadNotificationListener;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.HandlerResultMessage;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * The Class DialogUploadStream.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Sep 24, 2015
 */
public class DialogUpload extends FormPanel implements HasWorskpaceUploadNotificationListener {

	/** The file upload. */
	// protected final FormPanel formPanel = new FormPanel();
	protected FileUpload fileUpload;

	/** The json client keys. */
	protected Hidden jsonClientKeys;

	/** The fake uploaders. */
	protected List<FileUploadingState> fakeUploaders = new ArrayList<FileUploadingState>();

	/** The controller. */
	protected WorkspaceUploaderListenerController controller = new WorkspaceUploaderListenerController();

	/** The timer. */
	protected TimerUpload timer;

	protected String fieldName;

	/**
	 * The Enum UPLOAD_TYPE.
	 *
	 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Sep 11, 2015
	 */
	private HTML UPLOADING = new HTML("Uploading...");

	/** The vertical main panel. */
	protected VerticalPanel verticalMainPanel;

	/** The purged uploading file name. */
	private String purgedUploadingFileName;

	/** The upv. */
	private UploaderProgressView upv;

	/**
	 * Instantiates a new dialog upload stream.
	 */
	public DialogUpload() {
		fileUpload = new FileUpload();
		setAction(ConstantsMPFormBuilder.METADATA_FORM_BUILDER_UPLOADING_SERVLET);
		setEncoding(FormPanel.ENCODING_MULTIPART);
		setMethod(FormPanel.METHOD_POST);
		setWidth("auto");

		// Create a panel to hold all of the form widgets.
		verticalMainPanel = new VerticalPanel();
		setWidget(verticalMainPanel);

		fileUpload.setName(ConstantsMPFormBuilder.UPLOAD_FORM_ELEMENT);

		// Add hidden parameters
		verticalMainPanel
				.add(new Hidden(ConstantsMPFormBuilder.CURR_GROUP_ID, GCubeClientContext.getCurrentContextId()));
		verticalMainPanel.add(
				new Hidden(ConstantsMPFormBuilder.UPLOAD_TYPE, ConstantsMPFormBuilder.THE_UPLOAD_TYPE.File.name()));

		verticalMainPanel.add(new Hidden(ConstantsMPFormBuilder.UPL_FIELD_NAME_FILEPATH, fieldName));

		initJsonClientKeys();
		verticalMainPanel.add(jsonClientKeys);
		verticalMainPanel.add(fileUpload);

		HorizontalPanel hp = new HorizontalPanel();
		hp.getElement().getStyle().setWidth(100, Unit.PCT);
		hp.getElement().getStyle().setMarginTop(5, Unit.PX);
		hp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		hp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		verticalMainPanel.add(hp);
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public VerticalPanel getPanel() {
		return verticalMainPanel;
	}

	/**
	 * Inits the json client keys.
	 */
	protected void initJsonClientKeys() {
		jsonClientKeys = new Hidden(ConstantsMPFormBuilder.CLIENT_UPLOAD_KEYS, "");
	}

	/**
	 * Bind events.
	 */
	public void bindEvents() {
		this.addHandlers();
	}

	/**
	 * Adds the handlers.
	 */
	protected void addHandlers() {

		// handle the post
		addSubmitCompleteHandler(new SubmitCompleteHandler() {

			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				GWT.log("onSubmitComplete single upload");
//				isStatusCompleted = true;
				String result = event.getResults();

				if (result == null) {
					removeLoading();
					Window.alert("An error occurred during file upload");
					return;
				}
				String strippedResult = new HTML(result).getText();
				final HandlerResultMessage resultMessage = HandlerResultMessage.parseResult(strippedResult);

				switch (resultMessage.getStatus()) {
				case ERROR:
					removeLoading();
					// timer.cancel();
					GWT.log("Error during upload " + resultMessage.getMessage());
					break;
				case UNKNOWN:
					removeLoading();
					// timer.cancel();
					GWT.log("Error during upload " + resultMessage.getMessage());
					break;
				case WARN: {
					GWT.log("Upload completed with warnings " + resultMessage.getMessage());
					removeLoading();
					// timer.cancel();
					break;
				}
				case SESSION_EXPIRED: {
					GWT.log("Upload aborted due to session expired: " + resultMessage.getMessage());
					Window.alert("Session expired, please reload the page");
					removeLoading();
					// timer.cancel();
					break;
				}
				case OK: {
////					removeLoading();
////					UploaderMonitor.getInstance().pollWorkspaceUploaderId(resultMessage.getMessage());
//
//					Timer t = new Timer() {
//						public void run() {
//							GWT.log("Upload started with id: "+resultMessage.getMessage());
//							UploaderMonitor.getInstance().pollWorkspaceUploaderId(resultMessage.getMessage());
//						}
//					};
//
//					t.schedule(250);
				}
				}
			}
		});

		// TODO NEVER USED, WE ARE USING MULTIPLE DIALOG UPLOAD
		addSubmitHandler(new SubmitHandler() {

			@Override
			public void onSubmit(SubmitEvent event) {
				GWT.log("SubmitEvent");
				onSubmitAction();
			}
		});

		fileUpload.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				GWT.log("btnSubmit click");
				if (fileUpload.getFilename() == null || !(fileUpload.getFilename().length() > 2)) {
					GWT.log("No file specified ");
					return;
				}

				GWT.log("fileUpload.getFilename() " + fileUpload.getFilename());
				/*
				 * TODO: recall: Some browser would write in fileUploadField.getValue()
				 * C:\fakepath\$fileName
				 */
				String normalizedFileName = fileUpload.getFilename();
				if (normalizedFileName.contains("\\")) {
					normalizedFileName = normalizedFileName.substring(normalizedFileName.lastIndexOf("\\") + 1); // remove
																													// C:\fakepath\
																													// if
																													// exists
				}

				purgedUploadingFileName = normalizedFileName;
				submitForm();
			}
		});
	}

	/**
	 * On submit action.
	 */
	protected void onSubmitAction() {
		GWT.log("SubmitEvent");
		try {
			if (upv != null)
				verticalMainPanel.remove(upv.getPanel());
		} catch (Exception e) {
			// TODO: handle exception
		}
		addLoading();
		enableButtons(false);
		// Only the first, just to manage single uploading
		FileUploadingState workspaceUploaderItem = fakeUploaders.get(0);
		upv = new UploaderProgressView(workspaceUploaderItem, workspaceUploaderItem.getFile().getFileName(), null);
		verticalMainPanel.add(upv.getPanel());
		removeLoading();
		timer = new TimerUpload(workspaceUploaderItem, upv);
		SingleUploadMonitor.getInstance().doStartPolling(timer);
		fakeUploaders.clear();
	}

	public void eraseLastUpload() {
		try {
			if (upv != null) {
				verticalMainPanel.remove(upv.getPanel());
			}
		} catch (Exception e) {

		}

		if (timer != null) {
			FileUploadingState upload = timer.getLastWorkspaceUploader();
			if (upload != null)
				timer.resetLastWorkspaceUploader();
		}
		
		fakeUploaders.clear();
		fileUpload.getElement().setNodeValue("");
		
		fileUpload.getElement().setPropertyObject("value", "");
	}

	/**
	 * Gets the file uploading state.
	 *
	 * @return the file uploading state
	 */
	public FileUploadingState getFileUploadingState() {
		if (timer != null)
			return timer.getLastWorkspaceUploader();

		return null;
	}

	/**
	 * Adds the loading.
	 */
	protected void addLoading() {
		verticalMainPanel.add(UPLOADING);
	}

	/**
	 * Removes the loading.
	 */
	protected void removeLoading() {
		try {
			verticalMainPanel.remove(UPLOADING);
		} catch (Exception e) {

		}
	}

	/**
	 * Enable buttons.
	 *
	 * @param bool the bool
	 */
	protected void enableButtons(boolean bool) {
//		btnUpload.setEnabled(bool);
//		btnCancel.setEnabled(bool);
//		upload.setEnabled(bool);
	}

	/**
	 * Submit form.
	 */
	public void submitForm() {
		submit();
	}

	/**
	 * Removes the item and submit form.
	 *
	 * @param itemId the item id
	 */
	protected void removeItemAndSubmitForm(String itemId) {
		/*
		 * AppControllerExplorer.rpcWorkspaceService.removeItem(itemId, new
		 * AsyncCallback<Boolean>() {
		 * 
		 * @Override public void onFailure(Throwable caught) { Info.display("Error",
		 * caught.getMessage());
		 * 
		 * 
		 * }
		 * 
		 * @Override public void onSuccess(Boolean result) { if(result){
		 * hiddenOverwrite.setValue("true"); submitForm(); }
		 * 
		 * }
		 * 
		 * });
		 */
	}

	/**
	 * Update item submit form.
	 *
	 * @param itemId the item id
	 */
	protected void updateItemSubmitForm(String itemId) {
		submitForm();
	}

	/**
	 * Adds the workspace upload notification listener.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gcube.portlets.widgets.workspaceuploader.client.
	 * WorkspaceUploadNotification.HasWorskpaceUploadNotificationListener#
	 * addWorkspaceUploadNotificationListener(org.gcube.portlets.widgets.
	 * workspaceuploader.client.WorkspaceUploadNotification.
	 * WorskpaceUploadNotificationListener)
	 */
	@Override
	public void addWorkspaceUploadNotificationListener(WorskpaceUploadNotificationListener handler) {
		controller.addWorkspaceUploadListener(handler);
	}

	/**
	 * Removes the workspace upload notification listener.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gcube.portlets.widgets.workspaceuploader.client.
	 * WorkspaceUploadNotification.HasWorskpaceUploadNotificationListener#
	 * removeWorkspaceUploadNotificationListener(org.gcube.portlets.widgets.
	 * workspaceuploader.client.WorkspaceUploadNotification.
	 * WorskpaceUploadNotificationListener)
	 */
	@Override
	public void removeWorkspaceUploadNotificationListener(WorskpaceUploadNotificationListener handler) {
		controller.removeWorkspaceUploadListener(handler);
	}

	/**
	 * Enable upload.
	 *
	 * @param bool the bool
	 */
	public void enableUpload(boolean bool) {
		fileUpload.setEnabled(bool);
	}
}
