package org.gcube.portlets.widgets.mpformbuilder.client.form.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderService;
import org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderServiceAsync;
import org.gcube.portlets.widgets.mpformbuilder.client.events.CloseCreationFormProfileEvent;
import org.gcube.portlets.widgets.mpformbuilder.client.events.CloseCreationFormProfileEventHandler;
import org.gcube.portlets.widgets.mpformbuilder.client.events.DeleteCustomFieldProfileEvent;
import org.gcube.portlets.widgets.mpformbuilder.client.events.DeleteCustomFieldProfileEventHandler;
import org.gcube.portlets.widgets.mpformbuilder.client.form.MetaDataField;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.GenericFormEventsListener;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.HasGenericFormListenerRegistration;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata.CategoryPanel;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata.CustomFieldEntryProfile;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata.MetaDataFieldSkeleton;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.resources.AddResourceToDataset;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.DialogUpload;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.CustomLegend;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.InfoIconsLabels;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.LoaderIcon;
import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.catalogue.OrganizationBeanProfile;
import org.gcube.portlets.widgets.mpformbuilder.shared.license.LicenseBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.CategoryWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.Form;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.Popover;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Create metadata form for ckan product.
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class CreateMetadataForm extends Composite implements HasGenericFormListenerRegistration {

	/** The ui binder. */
	private static EditMetadataFormUiBinder uiBinder = GWT.create(EditMetadataFormUiBinder.class);

	/** The listeners generic form events. */
	private List<GenericFormEventsListener> listenersGenericFormEvents = new ArrayList<GenericFormEventsListener>();

	/**
	 * The Interface EditMetadataFormUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Oct 1, 2020
	 */
	interface EditMetadataFormUiBinder extends UiBinder<Widget, CreateMetadataForm> {
	}

	/** The create dataset main panel. */
	@UiField
	HTMLPanel createDatasetMainPanel;

	/** The custom fields. */
	@UiField
	ControlGroup customFields;

	/** The custom fields group. */
	@UiField
	ControlGroup customFieldsGroup;

	/** The add custom field button. */
	@UiField
	Button addCustomFieldButton;

	/** The create button. */
	@UiField
	Button createButton;

	/** The create button. */
	@UiField
	Button editButton;

	/** The info block. */
//	@UiField Button resetButton;
	@UiField
	AlertBlock infoBlock;

	/** The on continue alert block. */
	@UiField
	AlertBlock onContinueAlertBlock;

	/** The on create alert block. */
	@UiField
	AlertBlock onCreateAlertBlock;

	/** The metadata fields panel. */
	@UiField
	VerticalPanel metadataFieldsPanel;

	@UiField
	VerticalPanel filesUploadedPanel;

	/** The metadata type listbox. */
	@UiField
	ListBox metadataTypeListbox;

	/** The form first step. */
	@UiField
	Form formFirstStep;

	/** The form third step. */
	@UiField
	Form formThirdStep;

	/** The selected profile. */
//	@UiField Button continueButton;
	@UiField
	Paragraph selectedProfile;

	/** The loader profiles. */
	@UiField
	LoaderIcon loaderProfiles;

	/** The loader profile information. */
	@UiField
	LoaderIcon loaderProfileInformation;
	// @UiField TagsPanel tagsPanel;

	/** The info icon types. */
	// info panels
	@UiField
	Icon infoIconTypes;

	/** The focus panel types. */
	@UiField
	FocusPanel focusPanelTypes;

	/** The popover types. */
	@UiField
	Popover popoverTypes;

	/** The info icon custom fields. */
	@UiField
	Icon infoIconCustomFields;

	/** The focus panel custom fields. */
	@UiField
	FocusPanel focusPanelCustomFields;

	/** The popover custom fields. */
	@UiField
	Popover popoverCustomFields;

	/** The metadata types control group. */
	@UiField
	ControlGroup metadataTypesControlGroup;

	/** The custom legend. */
	@UiField
	CustomLegend customLegend;

	/** The form builder service. */
	private final MetadataProfileFormBuilderServiceAsync formBuilderService = GWT
			.create(MetadataProfileFormBuilderService.class);

	/** The Constant NONE_PROFILE. */
	private static final String NONE_PROFILE = "none";

	/** The Constant ERROR_PRODUCT_CREATION. */
	// error/info messages
	protected static final String ERROR_PRODUCT_CREATION = "There was an error while trying to publish your item.";

	/** The Constant PRODUCT_CREATED_OK. */
	protected static final String PRODUCT_CREATED_OK = "Item correctly published!";

	/** The Constant TRYING_TO_CREATE_PRODUCT. */
	private static final String DATA_FORM_FILLED_IN_CORRECTLY = "Very well!!! Data form filled in correclty!";

	/** The tab panel. */
	// tab panel
	private TabPanel tabPanel;

	/** The resource form. */
	// add resource form
	private AddResourceToDataset resourceForm;

	/** The license bean. */
	// the licenses
	private List<LicenseBean> licenseBean;

	/** The event bus. */
	// event bus
	private HandlerManager uiBus = new HandlerManager(null);

	private HandlerManager appManagerBus;

	/** The custom field entries list. */
	// added custom field entries (by the user)
	private List<CustomFieldEntryProfile> customFieldEntriesList = new ArrayList<CustomFieldEntryProfile>();

	/** The list of metadata fields. */
	// the list of MetaDataField added
	private List<MetaDataField> listOfMetadataFields = new ArrayList<MetaDataField>();

	// dataset metadata bean
	// private DatasetBean receivedBean;

	/** The owner. */
	// the owner
	private String owner;

	/** The resources twin panel. */
	// resource table
	//private TwinColumnSelectionMainPanel resourcesTwinPanel;

	/** The popup opened ids. */
	// List of opened popup'ids
	private List<String> popupOpenedIds = new ArrayList<String>();

	/** The name title organization map. */
	// map of organization name title
	private Map<String, String> nameTitleOrganizationMap = new HashMap<String, String>();

	/** The scope. */
	private String scope;

	/** The generic resource secondary type. */
	private String genericResourceSecondaryType;

	/** The metadata profiles. */
	private List<MetaDataProfileBean> metadataProfiles;

	/** The form data bean. */
	private GenericDatasetBean formDataBean;

	private OPERATION operationPerfom;

	/**
	 * The Enum OPERATION.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Mar 15, 2023
	 */
	public enum OPERATION {
		NEW, UPDATE
	}

	private UploadedFilesBrowse uploadedFileBrowse;

	/**
	 * Invoked in the most general case.
	 *
	 * @param scope                        the scope
	 * @param genericResourceSecondaryType the generic resource secondary type
	 * @param eventBus                     the event bus
	 */
	public CreateMetadataForm(String scope, String genericResourceSecondaryType, HandlerManager eventBus) {
		this(eventBus);
		this.scope = scope;
		this.genericResourceSecondaryType = genericResourceSecondaryType;
		showLoadingProfiles(true);
		MetadataProfileFormBuilderServiceAsync.Util.getInstance().getProfilesInTheScope(scope,
				genericResourceSecondaryType, new AsyncCallback<List<MetaDataProfileBean>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(List<MetaDataProfileBean> profiles) {
						createDatasetFormBody(profiles, null);
						showLoadingProfiles(false);
					}

				});

	}

	/**
	 * Instantiates a new creates the dataset form.
	 *
	 * @param profiles  the profiles
	 * @param eventBus  the event bus
	 * @param operation the operation
	 */
	public CreateMetadataForm(List<MetaDataProfileBean> profiles, HandlerManager eventBus, OPERATION operation) {
		this(eventBus);
		this.operationPerfom = operation;
		showLoadingProfiles(true);
		createDatasetFormBody(profiles, null);
		showLoadingProfiles(false);
	}

	/**
	 * Instantiates a new creates the metadata form.
	 *
	 * @param profiles         the profiles
	 * @param eventBus         the event bus
	 * @param operation        the operation
	 * @param listFileUploaded the list file uploaded
	 */
	public CreateMetadataForm(List<MetaDataProfileBean> profiles, HandlerManager eventBus, OPERATION operation,
			List<? extends FileUploaded> listFileUploaded) {
		this(eventBus);
		this.operationPerfom = operation;
		showLoadingProfiles(true);
		createDatasetFormBody(profiles, listFileUploaded);
		showLoadingProfiles(false);
	}

	/**
	 * Show choose profile form.
	 *
	 * @param bool the bool
	 */
	private void showChooseProfileForm(boolean bool) {
		formFirstStep.setVisible(bool);
	}

	/**
	 * Instantiates a new creates the dataset form.
	 *
	 * @param eventBus the event bus
	 */
	private CreateMetadataForm(HandlerManager eventBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = eventBus;
		bind();
		prepareInfoIcons();
		showSelectedProfiles(false);
		showChooseProfileForm(false);
		showCustomFieldsEntries(false);
	}

	/**
	 * Show loading profiles.
	 *
	 * @param bool the bool
	 */
	private void showLoadingProfiles(boolean bool) {
		loaderProfiles.setText("Loading Profiles...");
		loaderProfiles.setVisible(bool);
	}

	/**
	 * Show loading profile information.
	 *
	 * @param bool the bool
	 */
	private void showLoadingProfileInformation(boolean bool) {
		loaderProfileInformation.setText("Loading Profile Information...");
		loaderProfileInformation.setVisible(bool);
	}

	/**
	 * Bind on events.
	 */
	private void bind() {
		// when a custom field is removed, remove it from the list
		uiBus.addHandler(DeleteCustomFieldProfileEvent.TYPE, new DeleteCustomFieldProfileEventHandler() {

			@Override
			public void onRemoveEntry(DeleteCustomFieldProfileEvent event) {
				customFieldEntriesList.remove(event.getRemovedEntry());
				customFields.remove(event.getRemovedEntry());
			}
		});

		// on close form
		uiBus.addHandler(CloseCreationFormProfileEvent.TYPE, new CloseCreationFormProfileEventHandler() {
			@Override
			public void onClose(CloseCreationFormProfileEvent event) {
				InfoIconsLabels.closeDialogBox(popupOpenedIds);
			}
		});
	}

	/**
	 * Creates the dataset form body.
	 *
	 * @param profiles         the profiles
	 * @param listFileUploaded the list file uploaded
	 */
	private void createDatasetFormBody(final List<MetaDataProfileBean> profiles,
			List<? extends FileUploaded> listFileUploaded) {

		if (profiles == null) {
			setAlertBlock("An unknow error occurred while retrieving types, sorry", AlertType.ERROR, true);
		} else {
			this.metadataProfiles = profiles;
			prepareMetadataList(profiles, listFileUploaded);
			metadataTypeListbox.setEnabled(true);
		}
	}

	/**
	 * Add the items to the listbox and put data into the metadataPanel.
	 *
	 * @param profiles         the profiles
	 * @param listFileUploaded the file uploaded
	 */
	private void prepareMetadataList(final List<MetaDataProfileBean> profiles,
			final List<? extends FileUploaded> listFileUploaded) {

		if (profiles != null && !profiles.isEmpty()) {
			GWT.log("Building form/s for profile/s: " + profiles);

			if (profiles.size() > 1)
				showChooseProfileForm(true);

			for (MetaDataProfileBean metadataBean : profiles) {
				metadataTypeListbox.addItem(metadataBean.getType());
			}

			// add handler on select
			metadataTypeListbox.addChangeHandler(new ChangeHandler() {

				@Override
				public void onChange(ChangeEvent event) {
					GWT.log("Profile type selection changed...");

					String selectedItemText = metadataTypeListbox.getSelectedItemText();
					metadataFieldsPanel.clear();
					if (selectedItemText.equals(NONE_PROFILE)) {
						metadataFieldsPanel.setVisible(false);
						formThirdStep.setVisible(false);
						selectedProfile.setText("Selected Profile is " + NONE_PROFILE);
						// receivedBean.setChosenType(null);
					} else {
						// receivedBean.setChosenType(selectedItemText);
						showLoadingProfileInformation(true);
						formThirdStep.setVisible(true);
						customLegend.setText("Insert Information for: " + metadataTypeListbox.getSelectedItemText(),
								true);
						selectedProfile.setText("Selected Profile is " + metadataTypeListbox.getSelectedItemText());
						addFields(selectedItemText, profiles, listFileUploaded);
						showLoadingProfileInformation(false);
					}
				}
			});

		} else {
			// just hide this listbox
			metadataTypesControlGroup.setVisible(false);
			metadataFieldsPanel.clear();
			listOfMetadataFields.clear();
			// receivedBean.setChosenType(null);
		}

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				if (profiles.size() == 1) {
					showChooseProfileForm(false);
					metadataTypeListbox.setSelectedValue(profiles.get(0).getType());
					customLegend.setText("Insert Information for: " + profiles.get(0).getType(), true);
					DomEvent.fireNativeEvent(Document.get().createChangeEvent(), metadataTypeListbox);
				}
			}
		});

	}

	/**
	 * Add fields of the selected metadata profile to the widget.
	 *
	 * @param selectedItem     the selected item
	 * @param listProfiles     the list profiles
	 * @param listFileUploaded the list file uploaded
	 */
	protected void addFields(String selectedItem, List<MetaDataProfileBean> listProfiles,
			List<? extends FileUploaded> listFileUploaded) {

		for (MetaDataProfileBean bean : listProfiles) {
			if (bean.getType().equals(selectedItem)) {

				// clear old data
				listOfMetadataFields.clear();

				// prepare the data
				List<MetadataFieldWrapper> fields = bean.getMetadataFields();
				List<CategoryWrapper> categories = bean.getCategories();

				GWT.log("There are " + categories.size() + " categories for profile " + bean.getTitle());

				if (categories == null || categories.isEmpty()) {
					for (MetadataFieldWrapper field : fields) {
						/*
						 * MetaDataFieldSkeleton fieldWidget; try { fieldWidget = new
						 * MetaDataFieldSkeleton(field, eventBus); metadataFieldsPanel.add(fieldWidget);
						 * listOfMetadataFields.add(fieldWidget); } catch (Exception e) {
						 * GWT.log("Unable to build such widget", e); }
						 */

						MetaDataField fieldWidget;
						try {
							fieldWidget = new MetaDataField(field, uiBus, operationPerfom);
							metadataFieldsPanel.add(fieldWidget);
							listOfMetadataFields.add(fieldWidget);
						} catch (Exception e) {
							GWT.log("Unable to build such widget", e);
						}
					}
				} else {

					// create the categories, then parse the fields. Fields do not belonging to a
					// category are put at the end
					for (CategoryWrapper categoryWrapper : categories) {
						if (categoryWrapper.getFieldsForThisCategory() != null
								&& categoryWrapper.getFieldsForThisCategory().size() > 0) {
							CategoryPanel cp = new CategoryPanel(categoryWrapper.getTitle(),
									categoryWrapper.getDescription());
							List<MetadataFieldWrapper> fieldsForThisCategory = categoryWrapper
									.getFieldsForThisCategory();
							fields.removeAll(fieldsForThisCategory);

							for (MetadataFieldWrapper metadataFieldWrapper : fieldsForThisCategory) {

								/*
								 * MetaDataFieldSkeleton fieldWidget; try { fieldWidget = new
								 * MetaDataFieldSkeleton(metadataFieldWrapper, eventBus);
								 * cp.addField(fieldWidget); listOfMetadataFields.add(fieldWidget); } catch
								 * (Exception e) { GWT.log("Unable to build such widget", e); }
								 */

								MetaDataField fieldWidget;
								try {
									fieldWidget = new MetaDataField(metadataFieldWrapper, uiBus, operationPerfom);
									cp.addField(fieldWidget);
									listOfMetadataFields.add(fieldWidget);
								} catch (Exception e) {
									GWT.log("Unable to build such widget", e);
								}
							}
							metadataFieldsPanel.add(cp);
						}
					}

					// add the remaining one at the end of the categories
					CategoryPanel extrasCategory = new CategoryPanel("Other", null);
					for (MetadataFieldWrapper field : fields) {

						/*
						 * MetaDataFieldSkeleton fieldWidget; try { fieldWidget = new
						 * MetaDataFieldSkeleton(field, eventBus); extrasCategory.addField(fieldWidget);
						 * listOfMetadataFields.add(fieldWidget); } catch (Exception e) {
						 * GWT.log("Unable to build such widget", e); }
						 */

						MetaDataField fieldWidget;
						try {
							fieldWidget = new MetaDataField(field, uiBus, operationPerfom);
							extrasCategory.addField(fieldWidget);
							listOfMetadataFields.add(fieldWidget);
						} catch (Exception e) {
							GWT.log("Unable to build such widget", e);
						}
					}
					metadataFieldsPanel.add(extrasCategory);
				}
				metadataFieldsPanel.setVisible(true);
			}
		}

		if (listFileUploaded != null && listFileUploaded.size() > 0) {
			filesUploadedPanel.setVisible(true);
			uploadedFileBrowse = new UploadedFilesBrowse(listFileUploaded);
			filesUploadedPanel.add(uploadedFileBrowse);
		}
	}

	/**
	 * Adds the custom field event.
	 *
	 * @param e the e
	 */
	@UiHandler("addCustomFieldButton")
	void addCustomFieldEvent(ClickEvent e) {

		CustomFieldEntryProfile toAdd = new CustomFieldEntryProfile(uiBus, "", "", true);
		customFieldEntriesList.add(toAdd);
		customFields.add(toAdd);

	}

	/**
	 * Creates the dataset event.
	 *
	 * @param e the e
	 */
	@UiHandler("createButton")
	void createDatasetEvent(ClickEvent e) {

		String errorMessage = areProfileDataValid();

		if (errorMessage != null) {
			alertOnCreate("Please check the inserted values and the mandatory fields [" + errorMessage + "]",
					AlertType.ERROR, true);
		} else {

			// Set<String> tags = new HashSet<String>(tagsPanel.getTags());
			List<OrganizationBeanProfile> groups = new ArrayList<OrganizationBeanProfile>();
			List<OrganizationBeanProfile> groupsToForceCreation = new ArrayList<OrganizationBeanProfile>();
			LinkedHashMap<String, List<String>> customFieldsMap = new LinkedHashMap<String, List<String>>();
			List<FileUploaded> listFilesUploaded = new ArrayList<FileUploaded>();

			// prepare custom fields
			for (MetaDataField metaField : listOfMetadataFields) {

				for (MetaDataFieldSkeleton field : metaField.getListOfMetadataFields()) {

					switch (field.getField().getType()) {
					case File:
						DialogUpload dUpload = (DialogUpload) field.getHolder();
						// adding it only if exists
						if (dUpload.getFileUploadingState() != null)
							listFilesUploaded.add(dUpload.getFileUploadingState().getFile());
						break;

					default:
						List<String> valuesForField = field.getFieldCurrentValue();
						if (!valuesForField.isEmpty()) {
							String key = field.getFieldNameQualified();
							List<String> valuesForThisField = null;
							if (customFieldsMap.containsKey(key))
								valuesForThisField = customFieldsMap.get(key);
							else
								valuesForThisField = new ArrayList<String>();

							valuesForThisField.addAll(valuesForField);
							customFieldsMap.put(key, valuesForThisField);

							// get also tag/group if it is the case for this field
//							List<String> tagsField = field.getTagFromThisField();
//							if(tagsField != null)
//								tags.addAll(tagsField);

							List<String> groupsTitle = field.getGroupTitleFromThisGroup();
							if (groupsTitle != null) {
								for (String groupTitle : groupsTitle) {
									if (field.isGroupToForce())
										groupsToForceCreation.add(new OrganizationBeanProfile(groupTitle, groupTitle, false,
												field.isPropagateUp()));
									else
										groups.add(new OrganizationBeanProfile(groupTitle, groupTitle, false,
												field.isPropagateUp()));
								}
							}
						}
						break;
					}
				}
			}

			for (CustomFieldEntryProfile customEntry : customFieldEntriesList) {
				String key = customEntry.getKey();
				String value = customEntry.getValue();
				if (value != null && !value.isEmpty()) {
					List<String> valuesForThisField = null;
					if (customFieldsMap.containsKey(key))
						valuesForThisField = customFieldsMap.get(key);
					else
						valuesForThisField = new ArrayList<String>();
					valuesForThisField.add(value);
					customFieldsMap.put(key, valuesForThisField);
				}

			}

			GenericDatasetBean gdb = new GenericDatasetBean();
			gdb.setMetadataProfileList(metadataProfiles);
			gdb.setFormDataEntryFields(customFieldsMap);

			// THIS IS TRUE IN UPDATE MODE
			if (uploadedFileBrowse != null) {
				List<FileUploaded> rFilesUploaded = uploadedFileBrowse.getListRemainingFileUploaded();
				if (rFilesUploaded != null) {
					GWT.log("Added to FilesUploaded the remaining (current content) files: " + rFilesUploaded);
					listFilesUploaded.addAll(rFilesUploaded);
				}
			}

			gdb.setFilesUploaded(listFilesUploaded);
			formDataBean = gdb;

			// alert
			alertOnCreate(DATA_FORM_FILLED_IN_CORRECTLY, AlertType.SUCCESS, false);

			createButton.setEnabled(false);
			createButton.setVisible(false);
			editButton.setVisible(true);
			disableDatasetFields(true);
			notifyValidForm(gdb);
		}
	}

	/**
	 * Edits the button click handler.
	 *
	 * @param e the e
	 */
	@UiHandler("editButton")
	void editButtonClickHandler(ClickEvent e) {
		formDataBean = null;
		createButton.setEnabled(true);
		createButton.setVisible(true);
		editButton.setVisible(false);
		onCreateAlertBlock.setVisible(false);
		disableDatasetFields(false);
		notifyOnEditForm();
	}

	/**
	 * Checks if is form data valid.
	 *
	 * @return true, if is form data valid
	 */
	public boolean isFormDataValid() {

		// means no error
		if (areProfileDataValid() == null) {
			// means created button already fired
			if (formDataBean != null) {
				return true;
			} else {
				alertOnCreate("Please press the button 'Create' to confirm your data entries", AlertType.ERROR, true);
			}
		}
		return false;
	}

	/**
	 * Gets the form data bean.
	 *
	 * @return the form data bean
	 */
	public GenericDatasetBean getFormDataBean() {
		return formDataBean;
	}

	/**
	 * Prepare the info icons of all core metadata info.
	 */
	private void prepareInfoIcons() {

		// tags
		// tagsPanel.prepareIcon(popupOpenedIds);

		// profiles (or types)
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.PROFILES_INFO_ID_POPUP,
				InfoIconsLabels.PROFILES_INFO_TEXT, InfoIconsLabels.PROFILES_INFO_CAPTION, infoIconTypes, popoverTypes,
				focusPanelTypes, popupOpenedIds);

		// custom fields
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.CUSTOM_FIELDS_INFO_ID_POPUP,
				InfoIconsLabels.CUSTOM_FIELDS_INFO_TEXT, InfoIconsLabels.CUSTOM_FIELDS_INFO_CAPTION,
				infoIconCustomFields, popoverCustomFields, focusPanelCustomFields, popupOpenedIds);
	}

	/**
	 * Test if profile data are valid.
	 *
	 * @return the string
	 */
	private String areProfileDataValid() {

		for (MetaDataField metaField : listOfMetadataFields) {

			for (MetaDataFieldSkeleton field : metaField.getListOfMetadataFields()) {

				field.removeError();

				String error = field.isFieldValueValid();
				if (error != null) {
					field.showError();
					String errorMsg = field.getFieldNameOriginal() + " is not valid. Suggestion: " + error;
					notifyValidationError(null, errorMsg);
					return errorMsg;
				}
			}
		}

		return null;
	}

	/**
	 * On continue show alert box and enable buttons.
	 *
	 * @param text            the text
	 * @param type            the type
	 * @param hideAfterAWhile the hide after A while
	 */
	private void alertOnCreate(String text, AlertType type, boolean hideAfterAWhile) {

		// Window.alert("Called alertOnCreate");
		onCreateAlertBlock.setText(text);
		onCreateAlertBlock.setType(type);
		onCreateAlertBlock.setVisible(true);
		createButton.setEnabled(true);
		// goBackButtonSecondStep.setEnabled(true);

		if (hideAfterAWhile) {
			// hide after some seconds
			Timer t = new Timer() {

				@Override
				public void run() {

					onCreateAlertBlock.setVisible(false);

				}
			};
			t.schedule(10000);
		}
	}

	/**
	 * Reset form event.
	 *
	 * @param bool the bool
	 */
	/*
	 * @UiHandler("resetButton") void resetFormEvent(ClickEvent e){
	 * 
	 * // delete custom fields for (CustomFieldEntryProfile customField :
	 * customFieldEntriesList) { customField.removeFromParent(); }
	 * customFieldEntriesList.clear(); }
	 */

	/**
	 * Disable dataset editable fields once the dataset has been successfully
	 * created.
	 */
	protected void disableDatasetFields(boolean bool) {

		// tagsPanel.freeze();
		addCustomFieldButton.setEnabled(!bool);
		metadataTypeListbox.setEnabled(!bool);

		for (CustomFieldEntryProfile ce : customFieldEntriesList)
			ce.freeze(!bool);

		// disable profile fields
		for (MetaDataField metaField : listOfMetadataFields) {
			for (MetaDataFieldSkeleton field : metaField.getListOfMetadataFields()) {
				field.freeze(bool);
			}

		}

//		// freeze table of resources
//		if (resourcesTwinPanel != null)
//			resourcesTwinPanel.freeze(bool);

		// TRUE in UPDATE MODE - freeze manage of current content (means file already
		// uploaded)
		if (uploadedFileBrowse != null) {

			uploadedFileBrowse.enableManageOfContent(!bool);

		}
	}

	/**
	 * change alert block behavior.
	 *
	 * @param textToShow the text to show
	 * @param type       the type
	 * @param visible    the visible
	 */
	private void setAlertBlock(String textToShow, AlertType type, boolean visible) {

		infoBlock.setText(textToShow);
		infoBlock.setType(type);
		infoBlock.setVisible(visible);

	}

	/**
	 * Show custom fields entries.
	 *
	 * @param show the show
	 */
	public void showCustomFieldsEntries(boolean show) {
		this.customFields.setVisible(show);
		this.customFieldsGroup.setVisible(show);
	}

	/**
	 * Show selected profiles.
	 *
	 * @param show the show
	 */
	public void showSelectedProfiles(boolean show) {
		this.selectedProfile.setVisible(show);
	}

	/**
	 * Adds the listener.
	 *
	 * @param listener the listener
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gcube.portlets.widgets.mpformbuilder.client.form.generic.
	 * GenericFormEvents.HasGenericFormListenerRegistration#addListener(org.gcube.
	 * portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.
	 * GenericFormEventsListener)
	 */
	@Override
	public void addListener(GenericFormEventsListener listener) {
		if (listener != null) {
			if (listenersGenericFormEvents.contains(listener))
				return;

			listenersGenericFormEvents.add(listener);
		}
	}

	/**
	 * Removes the listener.
	 *
	 * @param listener the listener
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gcube.portlets.widgets.mpformbuilder.client.form.generic.
	 * GenericFormEvents.HasGenericFormListenerRegistration#removeListener(org.gcube
	 * .portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.
	 * GenericFormEventsListener)
	 */
	@Override
	public void removeListener(GenericFormEventsListener listener) {

		if (listener != null)
			listenersGenericFormEvents.remove(listener);
	}

	/**
	 * Notify valid form.
	 *
	 * @param genericDatasetBean the generic dataset bean
	 */
	private void notifyValidForm(GenericDatasetBean genericDatasetBean) {
		for (GenericFormEventsListener listener : listenersGenericFormEvents) {
			listener.onFormDataValid(genericDatasetBean);
		}
	}

	/**
	 * Notify valid form.
	 */
	private void notifyOnEditForm() {
		for (GenericFormEventsListener listener : listenersGenericFormEvents) {
			listener.onFormDataEdit();
		}
	}

	/**
	 * Notify validation error.
	 *
	 * @param throwable the throwable
	 * @param errorMsg  the error msg
	 */
	private void notifyValidationError(Throwable throwable, String errorMsg) {
//		listenersSize();
		for (GenericFormEventsListener listener : listenersGenericFormEvents) {
			listener.onValidationError(throwable, errorMsg);
		}
	}

	/**
	 * Gets the creates the button.
	 *
	 * @return the creates the button
	 */
//	public Button getCreateButton() {
//		return createButton;
//	}

	/**
	 * Gets the metadata profiles.
	 *
	 * @return the metadata profiles
	 */
	public List<MetaDataProfileBean> getMetadataProfiles() {
		return metadataProfiles;
	}

}