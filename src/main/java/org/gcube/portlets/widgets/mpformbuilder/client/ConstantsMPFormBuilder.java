package org.gcube.portlets.widgets.mpformbuilder.client;

import com.google.gwt.core.client.GWT;

public class ConstantsMPFormBuilder {
	
	public static final String METADATA_FORM_BUILDER_UPLOADING_SERVLET = GWT.getModuleBaseURL() + "metadataProfileFormBuilderUploadServlet";
	public static final String CURR_GROUP_ID = "currGroupId";
	
	//public static final String CURR_USER_ID = "currUserId";
	public static final String IS_OVERWRITE = "isOverwrite";
	public static final String UPLOAD_TYPE = "uploadType";

	public static enum THE_UPLOAD_TYPE {File, Archive};
	public static final String ID_FOLDER = "idFolder";
	public static final String UPLOAD_FORM_ELEMENT = "uploadFormElement";

	public static final String CLIENT_UPLOAD_KEYS = "client_upload_keys";
	public static final String CANCEL_UPLOAD = "cancel_upload";
	public static final String JSON_CLIENT_KEYS = "ClientKeys";
	public static final String UPL_FILENAME = "filename";
	public static final String UPL_CLIENT_KEY = "client_key";
	public static final String UPL_FIELD_NAME_FILEPATH = "fieldname_filepath";
	
	public static final int LIMIT_UPLOADS = 50;
	
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	
	public static final String HOURS_MINUTES_SEPARATOR = ":";
	
	public static final String TIME_FORMAT = "HH"+HOURS_MINUTES_SEPARATOR+"mm";
	
	public static final String FILE_UPLOADED_SESSION_ATTR = "FILE_UPLOADED";
	
	/**
	 * Log.
	 *
	 * @param txt the txt
	 */
	public static native void jslog(String txt) /*-{
	  console.log(txt)
	}-*/;



}
