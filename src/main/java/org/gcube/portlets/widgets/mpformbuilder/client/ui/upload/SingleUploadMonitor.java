package org.gcube.portlets.widgets.mpformbuilder.client.ui.upload;

import java.util.HashMap;
import java.util.Map;

import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.events.NotifyUploadEvent;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.upload.events.NotifyUploadEvent.UPLOAD_EVENT_TYPE;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;

public class SingleUploadMonitor {
	
	private static SingleUploadMonitor instance;
	private Map<String, TimerUpload> currentTimersRun = new HashMap<String, TimerUpload>();
	
	/**
	 * Gets the single instance of UploaderMonitor.
	 *
	 * @return single instance of UploaderMonitor
	 */
	public static synchronized SingleUploadMonitor getInstance() {
		if (instance == null){
			instance = new SingleUploadMonitor();
		}
		return instance;
	}
	
	/**
	 * Do start polling queue.
	 *
	 * @param index the index
	 * @param workspaceUploader 
	 * @param upv 
	 */
	public synchronized void doStartPolling(TimerUpload timer) {
		startTimer(timer);
	}
	
	/**
	 * Start timer.
	 *
	 * @param workspaceUploader the workspace uploader
	 */
	private void startTimer(TimerUpload timer){
		String clientUploadKey = timer.getInitWorkspaceUploader().getClientUploadKey();
		ConstantsMPFormBuilder.jslog("Starting new timer for key: "+clientUploadKey);
		currentTimersRun.put(clientUploadKey, timer);
		timer.scheduleRepeating(2000);
	}

	
	/**
	 * Removes the timer.
	 *
	 * @param clientKey the client key
	 */
	public synchronized void removeTimer(String clientKey){
		Timer timer = currentTimersRun.get(clientKey);
		if(timer!=null){
			currentTimersRun.remove(clientKey);
			GWT.log("Remove timer for key: "+clientKey+", performed correctly");
		}else
			GWT.log("Remove timer for key: "+clientKey+", skypped, already removed?");
	}

	/**
	 * Gets the timer.
	 *
	 * @param clientKey the client key
	 * @return the timer
	 */
	protected TimerUpload getTimer(String clientKey){
		return currentTimersRun.get(clientKey);
	}

	/**
	 * Notify upload completed.
	 *
	 * @param parentId the parent id
	 * @param itemId the item id
	 */
	protected void notifyOverwriteCompleted(String parentId, String itemId){
		GWT.log("notifyOverwriteCompleted in monitor");
		WorkspaceUploaderListenerController.getEventBus().fireEvent(new NotifyUploadEvent(UPLOAD_EVENT_TYPE.OVERWRITE_COMPLETED, parentId, itemId));
	}

	/**
	 * Notify upload completed.
	 *
	 * @param parentId the parent id
	 * @param itemId the item id
	 */
	protected void notifyUploadCompleted(String parentId, String itemId){
		GWT.log("notifyUploadCompleted in monitor");
		WorkspaceUploaderListenerController.getEventBus().fireEvent(new NotifyUploadEvent(UPLOAD_EVENT_TYPE.UPLOAD_COMPLETED, parentId, itemId));
	}

	/**
	 * Notify upload aborted.
	 *
	 * @param parentId the parent id
	 * @param itemId the item id
	 */
	protected void notifyUploadAborted(String parentId, String itemId){
		WorkspaceUploaderListenerController.getEventBus().fireEvent(new NotifyUploadEvent(UPLOAD_EVENT_TYPE.ABORTED, parentId, itemId));
	}

	/**
	 * Notify upload error.
	 *
	 * @param parentId the parent id
	 * @param itemId the item id
	 * @param t the t
	 */
	protected void notifyUploadError(String parentId, String itemId, String error, Throwable t){
		WorkspaceUploaderListenerController.getEventBus().fireEvent(new NotifyUploadEvent(UPLOAD_EVENT_TYPE.FAILED, parentId, itemId, error, t));
	}

}
