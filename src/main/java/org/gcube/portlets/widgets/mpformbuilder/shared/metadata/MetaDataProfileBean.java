package org.gcube.portlets.widgets.mpformbuilder.shared.metadata;

import java.io.Serializable;
import java.util.List;

/**
 * The Class MetaDataProfileBean.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 3, 2022
 */
public class MetaDataProfileBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8307518917648131477L;
	private String type;
	private String title;
	private List<CategoryWrapper> categories;
	private List<MetadataFieldWrapper> metadataFields;

	/**
	 * Instantiates a new meta data profile bean.
	 */
	public MetaDataProfileBean() {
		super();
	}

	/**
	 * Instantiates a new meta data profile bean.
	 *
	 * @param type           the type
	 * @param title          the title
	 * @param metadataFields the metadata fields
	 * @param categories     the categories
	 */
	public MetaDataProfileBean(String type, String title, List<MetadataFieldWrapper> metadataFields,
			List<CategoryWrapper> categories) {
		super();
		this.type = type;
		this.title = title;
		this.categories = categories;
		this.metadataFields = metadataFields;
	}

	public String getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}

	public List<CategoryWrapper> getCategories() {
		return categories;
	}

	public List<MetadataFieldWrapper> getMetadataFields() {
		return metadataFields;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCategories(List<CategoryWrapper> categories) {
		this.categories = categories;
	}

	public void setMetadataFields(List<MetadataFieldWrapper> metadataFields) {
		this.metadataFields = metadataFields;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MetaDataProfileBean [type=");
		builder.append(type);
		builder.append(", title=");
		builder.append(title);
		builder.append(", categories=");
		builder.append(categories);
		builder.append(", metadataFields=");
		builder.append(metadataFields);
		builder.append("]");
		return builder.toString();
	}

}
