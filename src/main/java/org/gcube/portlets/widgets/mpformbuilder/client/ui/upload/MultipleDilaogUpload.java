/**
 *
 */
package org.gcube.portlets.widgets.mpformbuilder.client.ui.upload;

import java.util.ArrayList;
import java.util.Arrays;

import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FilePath;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState.UPLOAD_STATUS;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.HandlerResultMessage;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;

/**
 * The Class MultipleDilaogUploadStream.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Oct 2, 2015
 */
public class MultipleDilaogUpload extends DialogUpload {

	/** The Constant FILE_DELEMITER. */
	public static final String FILE_DELEMITER = ";";

	/** The file upload ID. */
	private String fileUploadID;

	/** The instance. */
	// private MultipleDNDUpload dnd;
	public MultipleDilaogUpload instance = this;

	/** The json keys. */
	private String jsonKeys;

	/** The id folder. */
	private String idFolder;


	/**
	 * Instantiates a new multiple dilaog upload.
	 *
	 * @param fieldName the field name used in the web form
	 */
	public MultipleDilaogUpload(String fieldName) {
		this.fileUploadID = GenerateUUID.get(10, 16); // is tagID
		this.fileUpload.getElement().setId(fileUploadID);
		super.setFieldName(fieldName);
		this.addHandlers();
	}

	/**
	 * Generate new upload client keys.
	 *
	 * @param filesSelected the files selected
	 * @return the list
	 */
	public void generateFakeUploaders(String filesSelected) {

		if (filesSelected == null || filesSelected.isEmpty())
			return;

		String[] files = filesSelected.split(FILE_DELEMITER);

		// NORMALIZE FILE NAMES
		for (int i = 0; i < files.length; i++) {
			String normalizedFileName = files[i];
			if (normalizedFileName.contains("\\")) {
				files[i] = normalizedFileName.substring(normalizedFileName.lastIndexOf("\\") + 1); // remove C:\fakepath
																									// if exists
			}
		}

		GWT.log("generating fake uploaders on: " + Arrays.asList(files.toString()));
		fakeUploaders = new ArrayList<FileUploadingState>(files.length);
		for (int i = 0; i < files.length; i++) {
			FileUploadingState fakeItem = new FileUploadingState();
			fakeItem.setClientUploadKey(GenerateUUID.get());
			fakeItem.setUploadStatus(UPLOAD_STATUS.WAIT);
			FileUploaded fakeFile = new FileUploaded();
			fakeFile.setFileName(files[i]);
			fakeItem.setFile(fakeFile);
			fakeFile.setFilePath(new FilePath(getFieldName())); // The Form Field Label is unique for single-multiple
																	// upload
			fakeUploaders.add(fakeItem);
		}

		GWT.log("fakeUploaders generated: " + fakeUploaders.toString());
	}

	/**
	 * Adds the handlers.
	 */
	@Override
	protected void addHandlers() {

		// handle the post
		addSubmitCompleteHandler(new SubmitCompleteHandler() {

			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				GWT.log("onSubmitComplete in multipleupload");

				// isStatusCompleted = true;
				String result = event.getResults();

				if (result == null) {
					removeLoading();
					Window.alert("An error occurred during file upload.");
//					new DialogResult(null, "Error during upload",
//							"An error occurred during file upload.").center();
					return;
				}
				String strippedResult = new HTML(result).getText();
				final HandlerResultMessage resultMessage = HandlerResultMessage.parseResult(strippedResult);

				switch (resultMessage.getStatus()) {
				case ERROR:
					removeLoading();
					// timer.cancel();
					GWT.log("Error during upload " + resultMessage.getMessage());
					break;
				case UNKNOWN:
					removeLoading();
					// timer.cancel();
					GWT.log("Error during upload " + resultMessage.getMessage());
					break;
				case WARN: {
					GWT.log("Upload completed with warnings " + resultMessage.getMessage());
					removeLoading();
					// timer.cancel();
					break;
				}
				case SESSION_EXPIRED: {
					GWT.log("Upload aborted due to session expired: " + resultMessage.getMessage());
					Window.alert("Session expired, please reload the page");
					removeLoading();
					// timer.cancel();
					break;
				}
				case OK: {
				}

				}
			}

		});

		addSubmitHandler(new SubmitHandler() {

			@Override
			public void onSubmit(SubmitEvent event) {
				GWT.log("SubmitEvent");
				addLoading();
				enableButtons(false);
				onSubmitAction();
			}
		});

		fileUpload.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				GWT.log("file upload change handler, browse returns...");
				if (fileUpload.getFilename() == null || fileUpload.getFilename().isEmpty()) {
					GWT.log("No file specified ");
					// MultipleDilaogUpload.this.hide();
					return;
				}

				String[] files = null;
				GWT.log("Current Uploader has id: " + fileUploadID);
				String filesSelected = getFilesSelected(fileUploadID, FILE_DELEMITER);
				GWT.log("getFilesSelected: " + filesSelected);
				files = filesSelected.split(FILE_DELEMITER);

				if (isLimitExceeded(files.length))
					return;

				// GENERATE NEW UPLOADERS
				generateFakeUploaders(filesSelected);
				GWT.log(fakeUploaders.toString());
				createJsonKeyForFiles();
				GWT.log(jsonKeys);

				if (jsonKeys == null) {
					Window.alert("Sorry an error occurred during file/s submit. Try again");
					return;
				}

				// ADD TO FORM PANEL
				// initJsonClientKeys();
				jsonClientKeys.setValue(jsonKeys);
				submitForm();
			}
		});

	}

	/**
	 * Checks if is limit exceeded.
	 *
	 * @param numbOfFiles the numb of files
	 * @return true, if is limit exceeded
	 */
	public boolean isLimitExceeded(int numbOfFiles) {

		if (numbOfFiles > ConstantsMPFormBuilder.LIMIT_UPLOADS) {
			Window.alert("Multiple upload limit is " + ConstantsMPFormBuilder.LIMIT_UPLOADS + " files");
			// MultipleDilaogUpload.this.hide();
			return true;
		}

		return false;
	}

	/**
	 * Adds the new submit to monitor.
	 */
	public void addNewSubmitToMonitor() {
		GWT.log("addNewSubmitToMonitor...");
		/*
		 * int queueIndex = UploaderMonitor.getInstance().newQueue(); for (final
		 * WorkspaceUploaderItem workspaceUploaderItem : fakeUploaders) {
		 * UploaderMonitor.getInstance().addNewUploaderToMonitorPanel(
		 * workspaceUploaderItem, workspaceUploaderItem.getFile().getFileName());
		 * setVisible(false); removeLoading();
		 * UploaderMonitor.getInstance().addNewUploaderToQueue(queueIndex,
		 * workspaceUploaderItem); //
		 * UploaderMonitor.getInstance().pollWorkspaceUploader(workspaceUploaderItem); }
		 * 
		 * UploaderMonitor.getInstance().doStartPollingQueue(queueIndex);
		 */
	}

	/**
	 * Creates the json key for files.
	 *
	 * @return the string
	 */
	protected void createJsonKeyForFiles() {

		try {
			JSONObject productObj = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			productObj.put(ConstantsMPFormBuilder.JSON_CLIENT_KEYS, jsonArray);
//			GWT.log("Creating json keys on fakeUploaders: "+fakeUploaders.toString());

			for (int i = 0; i < fakeUploaders.size(); i++) {
				FileUploadingState file = fakeUploaders.get(i);
				JSONObject obj = new JSONObject();
				// JSONObject innerObj = new JSONObject();
				obj.put(ConstantsMPFormBuilder.UPL_CLIENT_KEY, new JSONString(file.getClientUploadKey()));
				obj.put(ConstantsMPFormBuilder.UPL_FILENAME, new JSONString(file.getFile().getFileName()));
				obj.put(ConstantsMPFormBuilder.UPL_FIELD_NAME_FILEPATH, new JSONString(file.getFile().getFilePath().getFormFieldLabel()));
				// obj.put(file.getClientUploadKey(), new
				// JSONString(file.getFile().getFileName()));
				jsonArray.set(i, obj);
			}

			jsonKeys = productObj.toString();
			GWT.log("updated jsonKeys: " + jsonKeys);
		} catch (Exception e) {
			GWT.log("error " + e.getMessage());
			jsonKeys = null;
		}
	}

	/**
	 * Submit form.
	 */
	@Override
	public void submitForm() {
		submit();
	}

	/**
	 * Gets the files selected.
	 *
	 * @param iFrameName the i frame name
	 * @return the files selected
	 */
	private static native String stopIFrame(final String iFrameName) /*-{
		console.log("iFrameName: " + iFrameName);
		//	   	var iframe= window.frames[iFrameName];
		var iframe = $wnd.$('iframe[name=' + iFrameName + ']', parent.document)[0];
		var iframewindow = iframe.contentWindow ? iframe.contentWindow
				: iframe.contentDocument.defaultView;
		if (iframe == null)
			console.log("iframe is null");
		else
			console.log("iframe is not null");

		if (navigator.appName == 'Microsoft Internet Explorer'
				&& iframewindow.document.execCommand) { // IE browsers
			console.log("IE browsers");
			iframewindow.document.execCommand("Stop");
		} else { // other browsers
			console.log("other browsers");
			iframewindow.stop();
		}

	}-*/;

	/**
	 * Gets the files selected.
	 *
	 * @param tagId         the tag id
	 * @param fileDelimiter the file delimiter
	 * @return the files selected
	 */
	public static native String getFilesSelected(final String tagId, final String fileDelimiter) /*-{
		var count = $wnd.$("#" + tagId)[0].files.length;
		//console.log(count);
		var out = "";

		for (i = 0; i < count; i++) {
			var file = $wnd.$("#" + tagId)[0].files[i];
			//	        out += file.name + fileDelimiter + file.size + fileDelimiter;
			out += file.name + fileDelimiter;
		}
		return out;
	}-*/;
}
