package org.gcube.portlets.widgets.mpformbuilder.shared.catalogue;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The Class OrganizationBeanProfile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 15, 2024
 */
public class OrganizationBeanProfile implements Serializable, IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2078766102599662775L;
	private String title;
	private String name;
	private boolean isOrganization;
	private boolean propagateUp; // an item linked to this group has to be added on the whole hierarchy chain

	/**
	 * Instantiates a new organization bean profile.
	 */
	public OrganizationBeanProfile() {
	}

	/**
	 * Instantiates a new organization bean profile.
	 *
	 * @param title          the title
	 * @param name           the name
	 * @param isOrganization the is organization
	 */
	public OrganizationBeanProfile(String title, String name, boolean isOrganization) {
		this.title = title;
		this.name = name;
		this.isOrganization = isOrganization;
	}

	/**
	 * Instantiates a new organization bean profile.
	 *
	 * @param title          the title
	 * @param name           the name
	 * @param isOrganization the is organization
	 * @param propagateUp    the propagate up
	 */
	public OrganizationBeanProfile(String title, String name, boolean isOrganization, boolean propagateUp) {
		this.title = title;
		this.name = name;
		this.isOrganization = isOrganization;
		this.propagateUp = propagateUp;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Checks if is organization.
	 *
	 * @return true, if is organization
	 */
	public boolean isOrganization() {
		return isOrganization;
	}

	/**
	 * Sets the organization.
	 *
	 * @param isOrganization the new organization
	 */
	public void setOrganization(boolean isOrganization) {
		this.isOrganization = isOrganization;
	}

	/**
	 * Checks if is propagate up.
	 *
	 * @return true, if is propagate up
	 */
	public boolean isPropagateUp() {
		return propagateUp;
	}

	/**
	 * Sets the propagate up.
	 *
	 * @param propagateUp the new propagate up
	 */
	public void setPropagateUp(boolean propagateUp) {
		this.propagateUp = propagateUp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrganizationBeanProfile [title=");
		builder.append(title);
		builder.append(", name=");
		builder.append(name);
		builder.append(", isOrganization=");
		builder.append(isOrganization);
		builder.append(", propagateUp=");
		builder.append(propagateUp);
		builder.append("]");
		return builder.toString();
	}

}
