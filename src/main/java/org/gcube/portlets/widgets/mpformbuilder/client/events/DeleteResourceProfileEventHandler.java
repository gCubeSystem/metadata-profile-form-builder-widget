package org.gcube.portlets.widgets.mpformbuilder.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The delete event handler
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public interface DeleteResourceProfileEventHandler extends EventHandler{
	void onDeletedResource(DeleteResourceProfileEvent deleteResourceEvent);
}
