package org.gcube.portlets.widgets.mpformbuilder.shared.catalogue;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * A resource element bean. Contains part of the logic used into the TwinColumn
 * widget
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class ResourceElementBeanProfile implements Serializable, IsSerializable {

	private static final long serialVersionUID = -1230871392599580669L;
	private String name;
	private String editableName;
	private boolean toBeAdded;
	private boolean isFolder;
	private String fullPath;
	private String originalIdInWorkspace;
	private String mimeType;
	private String url;
	private String description;
	private String organizationNameDatasetParent; // the organization name in which the parent dataset was created
	private ResourceElementBeanProfile parent;
	private List<ResourceElementBeanProfile> children;

	/**
	 * Default constructor
	 */
	public ResourceElementBeanProfile() {
	}

	/**
	 * Copy constructor
	 * 
	 * @param another
	 */
	public ResourceElementBeanProfile(ResourceElementBeanProfile another) {
		this.name = another.name;
		this.toBeAdded = another.toBeAdded;
		this.fullPath = another.fullPath;
		this.editableName = another.editableName;
		this.originalIdInWorkspace = another.originalIdInWorkspace;
		this.mimeType = another.mimeType;
		this.url = another.url;
		this.description = another.description;
		this.organizationNameDatasetParent = another.organizationNameDatasetParent;
	}

	/**
	 * @param identifier
	 * @param parentFolder
	 * @param name
	 * @param movedToRight
	 * @param isFolder
	 */
	public ResourceElementBeanProfile(ResourceElementBeanProfile parent, String name, boolean isFolder,
			List<ResourceElementBeanProfile> children, String fullPath) {
		this.parent = parent;
		this.name = name;
		this.isFolder = isFolder;
		this.children = children;
		this.fullPath = fullPath;
	}

	/**
	 * @param name
	 * @param toBeAdded
	 * @param isFolder
	 * @param parent
	 * @param children
	 * @param fullPath
	 * @param originalIdInWorkspace
	 * @param mimeType
	 * @param url
	 * @param description
	 * @param organizationNameDatasetParent
	 */
	public ResourceElementBeanProfile(String name, boolean toBeAdded, boolean isFolder,
			ResourceElementBeanProfile parent, List<ResourceElementBeanProfile> children, String fullPath,
			String originalIdInWorkspace, String mimeType, String url, String description,
			String organizationNameDatasetParent) {
		super();
		this.name = name;
		this.toBeAdded = toBeAdded;
		this.isFolder = isFolder;
		this.parent = parent;
		this.children = children;
		this.fullPath = fullPath;
		this.originalIdInWorkspace = originalIdInWorkspace;
		this.mimeType = mimeType;
		this.url = url;
		this.description = description;
		this.organizationNameDatasetParent = organizationNameDatasetParent;
	}

	public ResourceElementBeanProfile getParent() {
		return parent;
	}

	public void setParent(ResourceElementBeanProfile parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isToBeAdded() {
		return toBeAdded;
	}

	public void setToBeAdded(boolean toBeAdded) {
		this.toBeAdded = toBeAdded;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOrganizationNameDatasetParent() {
		return organizationNameDatasetParent;
	}

	public void setOrganizationNameDatasetParent(String organizationNameDatasetParent) {
		this.organizationNameDatasetParent = organizationNameDatasetParent;
	}

	public boolean isFolder() {
		return isFolder;
	}

	public void setFolder(boolean isFolder) {
		this.isFolder = isFolder;
	}

	public List<ResourceElementBeanProfile> getChildren() {
		return children;
	}

	public void setChildren(List<ResourceElementBeanProfile> children) {
		this.children = children;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getOriginalIdInWorkspace() {
		return originalIdInWorkspace;
	}

	public void setOriginalIdInWorkspace(String originalIdInWorkspace) {
		this.originalIdInWorkspace = originalIdInWorkspace;
	}

	public String getEditableName() {
		return editableName;
	}

	public void setEditableName(String newName) {
		this.editableName = newName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResourceElementBeanProfile [name=");
		builder.append(name);
		builder.append(", editableName=");
		builder.append(editableName);
		builder.append(", toBeAdded=");
		builder.append(toBeAdded);
		builder.append(", isFolder=");
		builder.append(isFolder);
		builder.append(", fullPath=");
		builder.append(fullPath);
		builder.append(", originalIdInWorkspace=");
		builder.append(originalIdInWorkspace);
		builder.append(", mimeType=");
		builder.append(mimeType);
		builder.append(", url=");
		builder.append(url);
		builder.append(", description=");
		builder.append(description);
		builder.append(", organizationNameDatasetParent=");
		builder.append(organizationNameDatasetParent);
		builder.append(", parent=");
		builder.append(parent);
		builder.append(", children=");
		builder.append(children);
		builder.append("]");
		return builder.toString();
	}

}
