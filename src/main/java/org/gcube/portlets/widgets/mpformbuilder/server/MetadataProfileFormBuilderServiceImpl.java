package org.gcube.portlets.widgets.mpformbuilder.server;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderService;
import org.gcube.portlets.widgets.mpformbuilder.server.util.WsUtil;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState.UPLOAD_STATUS;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.UploadProgress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

// TODO: Auto-generated Javadoc
/**
 * The server side implementation of the RPC service.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 22, 2020
 */
@SuppressWarnings("serial")
public class MetadataProfileFormBuilderServiceImpl extends RemoteServiceServlet
		implements MetadataProfileFormBuilderService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(MetadataProfileFormBuilderServiceImpl.class);

	/**
	 * Gets the profiles in the scope.
	 *
	 * @param scope the scope
	 * @param genericResourceSecondaryType the generic resource secondary type
	 * @return the profiles in the scope
	 * @throws Exception the exception
	 */
	@Override
	public List<MetaDataProfileBean> getProfilesInTheScope(String scope,String genericResourceSecondaryType) throws Exception {
		LOG.info("Called getProfilesInTheScope with parameter scope: " +scope+ ", genericResourceSecondaryType: "+genericResourceSecondaryType);
		
		if(genericResourceSecondaryType==null || genericResourceSecondaryType.isEmpty())
			throw new Exception("The input parameter 'genericResourceSecondaryType' is not valid");
		
		List<MetaDataProfileBean> toReturn = new ArrayList<MetaDataProfileBean>();
		try {
			
			String evaluatedScope = scope == null || scope.isEmpty()?ScopeProvider.instance.get():scope; 
			LOG.debug("Evaluated scope is  " + scope);
			toReturn = MetadataDiscovery.getMetadataProfilesList(evaluatedScope, genericResourceSecondaryType);
		} catch (Exception e) {
			LOG.error("Failed to retrieve profiles for scope " +scope, e);
			throw e;
		}

		return toReturn;
	}
	

	/**
	 * Gets the profiles in the scope.
	 *
	 * @param scope the scope
	 * @param genericResourceSecondaryType the generic resource secondary type
	 * @param resourceName the resource name
	 * @return the profiles in the scope
	 * @throws Exception the exception
	 */
	@Override
	public List<MetaDataProfileBean> getProfilesInTheScopeForName(String scope,String genericResourceSecondaryType, String resourceName) throws Exception {
		LOG.info("Called getProfilesInTheScope with parameter scope: " +scope+ ", genericResourceSecondaryType: "+genericResourceSecondaryType, "resourceName: "+resourceName);
		
		if(genericResourceSecondaryType==null || genericResourceSecondaryType.isEmpty())
			throw new Exception("The input parameter 'genericResourceSecondaryType' is not valid");
		
		if(resourceName==null || resourceName.isEmpty())
			throw new Exception("The input parameter 'resourceName' is not valid");
		
		List<MetaDataProfileBean> toReturn = new ArrayList<MetaDataProfileBean>();
		try {
			
			String evaluatedScope = scope == null || scope.isEmpty()?ScopeProvider.instance.get():scope; 
			LOG.debug("Evaluated scope is  " + scope);
			toReturn = MetadataDiscovery.getMetadataProfilesList(evaluatedScope, genericResourceSecondaryType, resourceName);
		} catch (Exception e) {
			LOG.error("Failed to retrieve profiles for scope " +scope, e);
			throw e;
		}

		return toReturn;
	}
	
	/**
	 * Gets the profile for metadata.
	 *
	 * @param metadata the metadata
	 * @return the profile for metadata
	 * @throws Exception the exception
	 */
	@Override
	public MetaDataProfileBean getProfileForMetadata(String metadata) throws Exception {
		LOG.info("Called getProfileForMetadata with parameter metadata: " + metadata);
		MetaDataProfileBean toReturn = null;
		try {
			InputStream targetStream = new ByteArrayInputStream(metadata.getBytes());
			toReturn = MetadataDiscovery.getMetadataProfile(targetStream);
		} catch (Exception e) {
			LOG.error("Failed to retrieve profile for metadata " +metadata, e);
			throw e;
		}

		return toReturn;
	}

	/**
	 * Gets the upload status.
	 *
	 * @param identifier the identifier
	 * @return the upload status
	 * @throws Exception the exception
	 */
	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.workspaceuploader.client.WorkspaceUploaderService#getUploadStatus(java.lang.String)
	 */
	@Override
	public FileUploadingState getUploadStatus(String identifier) throws Exception {
		LOG.debug("Called getUploadStatus for id: "+identifier);
		
		if (identifier == null || identifier.isEmpty()) {
			String exception = "Invalid upload identifier, it is null or empty";
			LOG.error(exception);
			throw new Exception(exception);
		}
		
		FileUploadingState uploader = WsUtil.getWorkspaceUploaderInSession(getThreadLocalRequest(), identifier);

		if (uploader == null) {
			FileUploadingState waitingUpload = new FileUploadingState(identifier, null, UPLOAD_STATUS.WAIT, "Upload waiting..", new UploadProgress());
			waitingUpload.setClientUploadKey(identifier);
			return waitingUpload;
		}

//		getThreadIds();
		String progress = "";
		if(uploader.getUploadProgress()!=null && uploader.getUploadProgress().getLastEvent()!=null)
			progress = uploader.getUploadProgress().getLastEvent().toString();
		else
			progress = "upload progress is null or last event is null";

		LOG.debug("returning uploader: "+uploader.getClientUploadKey() +" status: "+uploader.getUploadStatus() +", file: "+uploader.getFile().toString() +", progress: "+progress);

		checkUploaderErasable(this.getThreadLocalRequest(), uploader);
		return uploader;

	}
	
	
	/**
	 * Purge files uploaded in the current session.
	 *
	 * @return number of files deleted. Null otherwise.
	 * @throws Exception the exception
	 */
	@Override
	public Integer purgeFilesUploaded() throws Exception {
		LOG.info("purgeFilesUploaded called");
		
		HttpSession httpSession = this.getThreadLocalRequest().getSession();
		
		int count = 0;
		List<FileUploaded> listFileUploaded = (List<FileUploaded>) httpSession.getAttribute(ConstantsMPFormBuilder.FILE_UPLOADED_SESSION_ATTR);
		if(listFileUploaded!=null) {
			LOG.info("found file uploded in session, removing it");
			count = 0;
			for (FileUploaded fileUploaded : listFileUploaded) {
				boolean deleted = FileUtil.deleteFile(fileUploaded.getTempSystemPath());
				if(deleted)
					count++;
			}
			LOG.info("reset of attribute: "+ConstantsMPFormBuilder.FILE_UPLOADED_SESSION_ATTR);
			httpSession.setAttribute(ConstantsMPFormBuilder.FILE_UPLOADED_SESSION_ATTR, null);
		}
		LOG.info("deleted "+count+" file/s uploded from temp system folder");
		return count;
	}


	/**
	 * Check uploader erasable.
	 *
	 * @param httpRequest the http request
	 * @param uploader the uploader
	 */
	private void checkUploaderErasable(final HttpServletRequest httpRequest, final FileUploadingState uploader){
		LOG.debug("Called check Uploader erasable...");

		if(uploader==null){
			LOG.error("Uploader is null, returning..");
			return;
		}

		final HttpSession session = httpRequest.getSession();

		new Thread(){
			@Override
			public void run() {
				try {

					LOG.debug("Uploader: "+uploader.getClientUploadKey() +", is erasable? "+uploader.isErasable());
					WsUtil.eraseWorkspaceUploaderInSession(session, uploader);
				}
				catch (Exception e) {
					LOG.warn("Error during checkUploaderErasable: ", e);
				}
			}
		}.start();
	}
	
}
