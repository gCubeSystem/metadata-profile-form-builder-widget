package org.gcube.portlets.widgets.mpformbuilder.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * Handler associated to the DeleteCustomFieldProfileEvent
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public interface DeleteCustomFieldProfileEventHandler extends EventHandler {
  void onRemoveEntry(DeleteCustomFieldProfileEvent event);
}
