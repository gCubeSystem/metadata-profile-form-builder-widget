package org.gcube.portlets.widgets.mpformbuilder.server;

import java.io.File;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class FileUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 21, 2020
 */
public class FileUtil {
	
	private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);
	
	/**
	 * Delete file.
	 *
	 * @param path the path
	 * @return true, if successful
	 */
	public static boolean deleteFile(String path) {
		LOG.info("called deleteFile for path: "+path);
		boolean deleted = false;
		if(path!=null) {
			try {
				File file = new File(path);
				deleted = Files.deleteIfExists(file.toPath());
				LOG.info("File "+path+" deleted? "+deleted);
			}catch (Exception e) {
				LOG.warn("Error on deleting file: "+path, e);
			}
		}else {
			LOG.warn("I cannot delete the file. NullPointer parameter 'path'");
		}
		
		return deleted;
	}

}
