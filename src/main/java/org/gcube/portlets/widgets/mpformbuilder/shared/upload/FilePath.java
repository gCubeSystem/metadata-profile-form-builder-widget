package org.gcube.portlets.widgets.mpformbuilder.shared.upload;

import java.io.Serializable;

/**
 * The Class FilePath.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 13, 2023
 */
public class FilePath implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1544808784879956412L;
	private String formFieldLabel; // It is the gcubeProfileFieldName registered in the UCD of Geoportal
	private String fieldName; // It is the fieldName registered in the UCD of Geoportal

	/**
	 * Instantiates a new file paths.
	 */
	public FilePath() {
	}

	/**
	 * Instantiates a new file path.
	 *
	 * @param formFieldLabel the form field label
	 */
	public FilePath(String formFieldLabel) {
		this.formFieldLabel = formFieldLabel;
	}

	/**
	 * Instantiates a new file path.
	 *
	 * @param formFieldLabel the form field label
	 * @param fieldName      the field name
	 */
	public FilePath(String formFieldLabel, String fieldName) {
		this.formFieldLabel = formFieldLabel;
		this.fieldName = fieldName;
	}

	/**
	 * Gets the form field label.
	 *
	 * @return the form field label
	 */
	public String getFormFieldLabel() {
		return formFieldLabel;
	}

	/**
	 * Sets the form field label.
	 *
	 * @param formFieldLabel the new form field label
	 */
	public void setFormFieldLabel(String formFieldLabel) {
		this.formFieldLabel = formFieldLabel;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FilePath [formFieldLabel=");
		builder.append(formFieldLabel);
		builder.append(", fieldName=");
		builder.append(fieldName);
		builder.append("]");
		return builder.toString();
	}

}
