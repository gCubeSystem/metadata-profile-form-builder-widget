package org.gcube.portlets.widgets.mpformbuilder.client.events;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata.CustomFieldEntryProfile;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Delete custom field event.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class DeleteCustomFieldProfileEvent  extends GwtEvent<DeleteCustomFieldProfileEventHandler> {
	public static Type<DeleteCustomFieldProfileEventHandler> TYPE = new Type<DeleteCustomFieldProfileEventHandler>();

	private CustomFieldEntryProfile removedEntry;

	public DeleteCustomFieldProfileEvent(CustomFieldEntryProfile removedEntry) {
		this.removedEntry = removedEntry;
	}
	
	public CustomFieldEntryProfile getRemovedEntry() {
		return removedEntry;
	}

	@Override
	public Type<DeleteCustomFieldProfileEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DeleteCustomFieldProfileEventHandler handler) {
		handler.onRemoveEntry(this);
	}
}
