/**
 *
 */
package org.gcube.portlets.widgets.mpformbuilder.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;
import org.gcube.portlets.widgets.mpformbuilder.server.util.WsUtil;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.AbstractUploadProgressListener;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FilePath;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState.UPLOAD_STATUS;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.HandlerResultMessage;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.MemoryUploadListener;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.UploadCanceledException;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.UploadProgressInputStream;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.UploadProgressListener;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class MetadataProfileBuilderUploadServlet.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it at ISTI-CNR
 * 
 *         Oct 5, 2020
 */
public class MetadataProfileFormBuilderUploadServlet extends HttpServlet implements Servlet {

	/** The Constant UNKNOWN_UNKNOWN. */
	public static final String UNKNOWN_UNKNOWN = "unknown/unknown";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1778008252774571216L;

	/** The Constant UTF_8. */
	protected static final String UTF_8 = "UTF-8";

	/** The Constant VRE_ID_ATTR_NAME. */
	public static final String VRE_ID_ATTR_NAME = "gcube-vreid";

	/** The Constant CURR_GROUP_ID. */
	public static final String CURR_GROUP_ID = ConstantsMPFormBuilder.CURR_GROUP_ID;

	// public static final String CURR_USER_ID =
	// ConstantsWorkspaceUploader.CURR_USER_ID;

	/** The Constant UPLOAD_FORM_ELEMENT. */
	public static final String UPLOAD_FORM_ELEMENT = ConstantsMPFormBuilder.UPLOAD_FORM_ELEMENT;

	/** The Constant CLIENT_UPLOAD_KEYS. */
	public static final String CLIENT_UPLOAD_KEYS = ConstantsMPFormBuilder.CLIENT_UPLOAD_KEYS;

	/** The Constant JSON_CLIENT_KEYS. */
	public static final String JSON_CLIENT_KEYS = ConstantsMPFormBuilder.JSON_CLIENT_KEYS;

	/** The Constant CANCEL_UPLOAD. */
	public static final String CANCEL_UPLOAD = ConstantsMPFormBuilder.CANCEL_UPLOAD;

	/** The Constant UPLOAD_TYPE. */
	public static final String UPLOAD_TYPE = ConstantsMPFormBuilder.UPLOAD_TYPE;

	public static final String FIELD_NAME_FILEPATH = ConstantsMPFormBuilder.UPL_FIELD_NAME_FILEPATH;

	public static final String FILENAME = ConstantsMPFormBuilder.UPL_FILENAME;

	public static final String JSON_CLIENT_KEY = ConstantsMPFormBuilder.UPL_CLIENT_KEY;

	/** The logger. */
	public static Logger logger = LoggerFactory.getLogger(MetadataProfileFormBuilderUploadServlet.class);

	/** The app engine. */
	private static boolean appEngine = false;

	public static final String JAVA_IO_TMPDIR = System.getProperty("java.io.tmpdir");

	/**
	 * Inits the.
	 *
	 * @throws ServletException the servlet exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		super.init();

		String appe = getInitParameter("appEngine");
		if (appe != null) {
			appEngine = "true".equalsIgnoreCase(appe);
		} else {
			appEngine = isAppEngine();
		}

		logger.debug("init: appEngine is " + appEngine);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("GET method in " + MetadataProfileFormBuilderUploadServlet.class.getName() + " is running");
		String clientUploadKey = request.getParameter(CLIENT_UPLOAD_KEYS);
		if (clientUploadKey == null) {
			sendError(response, "Internal error: UPLOAD KEY NOT FOUND");
			return;
		}
		logger.debug("GET method CLIENT_UPLOAD_KEY " + clientUploadKey);

		boolean cancelUpload = Boolean.parseBoolean(request.getParameter(CANCEL_UPLOAD));
		logger.debug("GET method CANCEL_UPLOAD " + cancelUpload);
		if (cancelUpload) {
			boolean cancelled = cancelUpload(request.getSession(), clientUploadKey);
			if (cancelled) {
				sendMessage(response, "Upload aborted " + clientUploadKey);
//        	  try {
////        		removeCurrentListener(request.getSession(), clientUploadKey);
//				WsUtil.eraseWorkspaceUploaderInSession(request.getSession(), clientUploadKey);
//			  }catch (Exception e) {
//				  logger.warn("An error occurred during removing cancelled upload from session ");
//			  }
			} else
				sendWarnMessage(response,
						"Upload aborted for id: " + clientUploadKey + " has skipped, already aborted or completed?");
		} else
			logger.debug(CANCEL_UPLOAD + " param not found");
		return;
	}

	/**
	 * {@inheritDoc}
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("POST on UploadServlet");
		boolean requestIsNull = request == null;
		logger.trace("[1] HttpServletRequest is: null? " + requestIsNull + ", URI: " + request.getRequestURI()
				+ ", ServerName: " + request.getServerName());

		if (!ServletFileUpload.isMultipartContent(request)) {
			logger.error("ERROR: multipart request not found");
			sendError(response, "ERROR: multipart request not found");
			return;
		}

		try {

			logger.info("UPLOAD-SERVLET starting");
			HttpSession session = request.getSession();
			logger.info("UPLOAD-SERVLET session: " + session);
			logger.debug("UPLOAD-SERVLET (" + session.getId() + ") new upload request received.");

			if (WsUtil.isSessionExpired(request)) {
				logger.error("SESSION_EXPIRED: session is expired");
				sendSessionExpired(response, "SESSION_EXPIRED: session is expired");
				return;
			}

			String uploadType = null;
			String fieldName = null;
			// String clientUploadKey = null;
			FileItemStream uploadItem = null;
			ArrayList<String> listClientUploadKeys = null;

			GCubeUser user = PortalContext.getConfiguration().getCurrentUser(request);
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload servletFileUpload = new ServletFileUpload(factory);

			/**
			 * An iterator to instances of <code>FileItemStream</code> parsed from the
			 * request, in the order that they were transmitted.
			 */
			FileItemIterator fileItemIterator = servletFileUpload.getItemIterator(request);

			int uploadItemsCnt = 0;
			String scopeGroupId = "";
			// GET FILE STREAM
			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();

				if (item.isFormField() && UPLOAD_TYPE.equals(item.getFieldName())) {
					uploadType = Streams.asString(item.openStream());
					logger.debug(UPLOAD_TYPE + " OK " + uploadType);
				}

				if (item.isFormField() && FIELD_NAME_FILEPATH.equals(item.getFieldName())) {
					fieldName = Streams.asString(item.openStream());
					logger.debug(FIELD_NAME_FILEPATH + " OK " + fieldName);
				}

//				if (item.isFormField() && IS_OVERWRITE.equals(item.getFieldName())){
//					try{
//						isOverwrite = Boolean.parseBoolean(Streams.asString(item.openStream()));
//						logger.debug("IS_OVERWRITE OK "+ isOverwrite);
//					}catch(Exception e){
//						//Silent exception;
//					}
//				}

				if (item.isFormField() && CLIENT_UPLOAD_KEYS.equals(item.getFieldName())) {
					String jsonClientUploadKey = Streams.asString(item.openStream());
					logger.debug(CLIENT_UPLOAD_KEYS + " OK " + jsonClientUploadKey);
					LinkedHashMap<String, JSONFileUploaded> mapKeys = parseJSONClientUploadKeys(jsonClientUploadKey);
					listClientUploadKeys = new ArrayList<String>(mapKeys.keySet());
					removeListenersIfDone(session, listClientUploadKeys);
					for (String clientUploadKey : listClientUploadKeys) {
						JSONFileUploaded fileUploaded = mapKeys.get(clientUploadKey);
						logger.debug("Read file: " + fileUploaded);
						String fileName = fileUploaded.getFileName() != null ? fileUploaded.getFileName()
								: "No filename " + new Random().nextInt();
						fieldName = fileUploaded.getFieldNameFilePath() != null ? fileUploaded.getFieldNameFilePath()
								: fieldName;
						FileUploadingState workspaceUploader = createNewWorkspaceUploader(clientUploadKey, fileName,
								user.getUsername(), fieldName);
						logger.debug("created " + workspaceUploader);
						saveWorkspaceUploaderStatus(workspaceUploader, UPLOAD_STATUS.WAIT,
								"Uploading " + fileName + " at 0%", request.getSession());
					}
				}

				if (item.isFormField() && CURR_GROUP_ID.equals(item.getFieldName())) {
					scopeGroupId = Streams.asString(item.openStream());
					logger.debug("currentGroupId passed as parameter = " + scopeGroupId);
					logger.debug("currentGroupId into PortalContext scope= "
							+ PortalContext.getConfiguration().getCurrentScope(scopeGroupId));
				}

//				if (item.isFormField() && CURR_USER_ID.equals(item.getFieldName())){
//					currUserId = Streams.asString(item.openStream());
//					logger.debug("currUserId passed as parameter = " + currUserId);
//					logger.debug("currUserinto PortalContext = " + PortalContext.getConfiguration().getCurrentUser(request));
//				}

				// MUST BE THE LAST PARAMETER TRASMITTED
				if (UPLOAD_FORM_ELEMENT.equals(item.getFieldName())) {
					uploadItem = item;
					logger.debug("UPLOAD_FORM_ELEMENT OK " + uploadItem.getName() + " scopeGroupId=" + scopeGroupId);
//					break;
					uploadData(user, scopeGroupId, request, response, uploadItem, uploadType,
							listClientUploadKeys.get(uploadItemsCnt));
					uploadItemsCnt++;
				}
			}

		} catch (FileUploadException e) {
			logger.error("Error processing request in upload servlet", e);
			sendError(response, "Internal error: Error during request processing");
			return;
		} catch (Exception e) {
			logger.error("Error processing request in upload servlet", e);
			sendError(response, "Internal error: Error during request processing");
			return;
		}
	}

	/**
	 * Removes the listener if done.
	 *
	 * @param session the session
	 * @param keys    the keys
	 */
	private void removeListenersIfDone(HttpSession session, List<String> keys) {

		for (String key : keys) {

			AbstractUploadProgressListener listener = getCurrentListener(session, key);
			if (listener != null) {
				logger.debug("Listener found");
				if (listener.isCanceled() || listener.getPercentage() >= 100) {
					logger.debug("Listener isCanceled or 100%, removing");
					removeCurrentListener(session, key);
				}
			} else
				logger.debug("Session id: " + session.getId() + " - " + key + " - Listener not found");
		}

	}

	/**
	 * Parses the json client upload keys.
	 *
	 * @param jsonClientUploadKeys the json client upload keys
	 * @return the linked hash map
	 * @throws FileUploadException the file upload exception
	 */
	@SuppressWarnings("rawtypes")
	private static LinkedHashMap<String, JSONFileUploaded> parseJSONClientUploadKeys(final String jsonClientUploadKeys)
			throws FileUploadException {
		JSONTokener tokener = new JSONTokener(jsonClientUploadKeys);
		JSONObject root;
		LinkedHashMap<String, JSONFileUploaded> keyFiles = null;
		try {

			root = new JSONObject(tokener);
			JSONArray jsonArray = root.getJSONArray(JSON_CLIENT_KEYS);
			keyFiles = new LinkedHashMap<String, JSONFileUploaded>(jsonArray.length());
			logger.debug("jsonArray :" + jsonArray.toString());
			for (int i = 0; i < jsonArray.length(); i++) {
				String objectString = jsonArray.getString(i);
				ObjectMapper om = new ObjectMapper();
				JSONFileUploaded jfu = om.readValue(objectString, JSONFileUploaded.class);
				keyFiles.put(jfu.clienKey, jfu);
				/*
				 * JSONObject object = jsonArray.getJSONObject(i); logger.debug("object :" +
				 * object); String key = (String) object.keys().next(); String value =
				 * object.getString(key); logger.debug("key :" + key + ", value: " + value);
				 * keyFiles.put(key, value);
				 */
			}

		} catch (JSONException | IOException e) {
			logger.error("An error occurred during parsing file names: " + keyFiles, e);
			throw new FileUploadException("An error occurred during parsing file names");
		}

		logger.debug("keyFiles: " + keyFiles);
		return keyFiles;
	}

	/**
	 * Upload data.
	 *
	 * @param user            the user
	 * @param scopeGroupId    the scope group id
	 * @param request         the request
	 * @param response        the response
	 * @param uploadItem      the upload item
	 * @param uploadType      the upload type
	 * @param clientUploadKey the client upload key
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	private void uploadData(GCubeUser user, String scopeGroupId, HttpServletRequest request,
			final HttpServletResponse response, final FileItemStream uploadItem, String uploadType,
			String clientUploadKey) throws ServletException, IOException {

		String fileName = uploadItem.getName();
		logger.info("Upload servlet parameters: [fileName: " + fileName + ", uploadType: " + uploadType
				+ ", clientUploadKey: " + clientUploadKey + "]");

		if (uploadType == null || uploadType.isEmpty()) {
			logger.error("Error processing request in upload servlet for: " + fileName + ". No upload type found");
			sendError(response, "Internal error: No upload type found");
			return;
		}

		if (clientUploadKey == null || clientUploadKey.isEmpty()) {
			logger.error(
					"Error processing request in upload servlet for: " + fileName + ". No client upload key found");
			sendError(response, "Internal error: No client upload key found");
			return;
		}

		// CLIENT UPLOAD IS THE KEY
//		WorkspaceUploaderItem workspaceUploader = createNewWorkspaceUploader(clientUploadKey,destinationId,fileName);
//		saveWorkspaceUploaderStatus(workspaceUploader, UPLOAD_STATUS.WAIT, "Uploading "+fileName+" at 0%", request.getSession());

		// RETRIVE WORKSPACE UPLOADER FROM SESSION

		FileUploadingState workspaceUploader = null;
		try {
			workspaceUploader = WsUtil.getWorkspaceUploaderInSession(request, clientUploadKey);
		} catch (Exception e) {
			logger.error("Error during workspace uploader retrieving", e);
			saveWorkspaceUploaderStatus(workspaceUploader, UPLOAD_STATUS.FAILED,
					"An error occurred during upload: " + fileName + ". Error processing request in upload servlet",
					request.getSession());
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Error processing request in upload servlet");
			return;
		}

		try {

			// Removing path from fileName
			String itemName = fileName;
			// Getting extension
			String extension = FilenameUtils.getExtension(itemName);
			logger.debug("extension: " + extension);
			// Getting base name
			String baseName = FilenameUtils.getBaseName(itemName); // Using base name in order to avoid Task #12470

			// Task #17152
			extension = extension.isEmpty() ? "" : "." + extension;

			itemName = baseName + extension;
			logger.debug("purged item name is: " + itemName);
			// SIZE
			Long size = getContentLength(request);
			logger.debug("size: " + size + " bytes");

			// Instancing the progress listener
			final AbstractUploadProgressListener uploadProgressListener = createNewListener(request.getSession(),
					clientUploadKey);
			final UploadProgressInputStream inputStream = new UploadProgressInputStream(uploadItem.openStream(), size);
			inputStream.addListener(uploadProgressListener);
			workspaceUploader.setUploadProgress(uploadProgressListener.getUploadProgress());

			saveWorkspaceUploaderInSession(workspaceUploader, request.getSession());

			String contentType = uploadItem.getContentType();
			logger.debug("Stream content type: " + contentType);

			// UPLOAD FILE. IT IS DEFAULT CASE
			logger.debug("Uploading file in the TEMP system folder...");
			workspaceUploader = createStreamToTempFile(request, workspaceUploader, inputStream, user.getUsername(),
					clientUploadKey, fileName, extension);

			saveWorkspaceUploaderInSession(workspaceUploader, request.getSession());

			if (workspaceUploader == null)
				throw new Exception("Error when creating uploader, it is null!");

			/*
			 * Incident #10095. Commented OK as response in order to avoid downloading of
			 * .dms file by Safari from MAC
			 */
			// sendMessage(response, workspaceUploader.getIdentifier());

		} catch (IOException e) {
			logger.error("Error creating elements, is it cancel?", e);
			saveWorkspaceUploaderStatus(workspaceUploader, UPLOAD_STATUS.FAILED, "Uploading error",
					request.getSession());
			sendError(response, "Internal error: An item with that name already exists");
			return;
//			sendError(response, "Internal error: An item with that name already exists");
//			return;
		} catch (Exception e) {
			logger.error("Error creating elements", e);
			saveWorkspaceUploaderStatus(workspaceUploader, UPLOAD_STATUS.FAILED, "Uploading error. " + e.getMessage(),
					request.getSession());
//			sendError(response, "Internal error: An error occurred on uploading the file, try again later");
//			return;
		} finally {
			// TODO
			removeCurrentListener(request.getSession(), clientUploadKey);
		}
	}

	/**
	 * Save workspace uploader status.
	 *
	 * @param workspaceUploader the workspace uploader
	 * @param status            the status
	 * @param description       the description
	 * @param session           the session
	 * @return the workspace uploader item
	 */
	private synchronized FileUploadingState saveWorkspaceUploaderStatus(FileUploadingState workspaceUploader,
			UPLOAD_STATUS status, String description, HttpSession session) {
		workspaceUploader.setUploadStatus(status);
		workspaceUploader.setStatusDescription(description);
		try {
			saveWorkspaceUploaderInSession(workspaceUploader, session);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return workspaceUploader;
	}

	/**
	 * Creates the new workspace uploader.
	 *
	 * @param clientUploadKey the client upload key
	 * @param fileName        the file name
	 * @param user            the user
	 * @param formFieldName   the form field name
	 * @return the workspace uploader item
	 */
	private FileUploadingState createNewWorkspaceUploader(String clientUploadKey, String fileName, String user,
			String formFieldName) {
		// CLIENT UPLOAD IS THE KEY
		FileUploadingState workspaceUploader = new FileUploadingState(clientUploadKey);
		workspaceUploader.setClientUploadKey(clientUploadKey);
		// Create File
		FileUploaded wsUploadFile = new FileUploaded(fileName, null, user, new FilePath(formFieldName));
		workspaceUploader.setFile(wsUploadFile);
		return workspaceUploader;
	}

	/**
	 * Save workspace uploader in session.
	 *
	 * @param workspaceUploader the workspace uploader
	 * @param httpSession       the http session
	 * @return the workspace uploader item
	 * @throws Exception the exception
	 */
	public static void saveWorkspaceUploaderInSession(FileUploadingState workspaceUploader, HttpSession httpSession)
			throws Exception {

		if (workspaceUploader != null) {
			try {
				WsUtil.putWorkspaceUploaderInSession(httpSession, workspaceUploader);
			} catch (Exception e) {
				logger.error(
						"Error during WorkspaceUploaderItem save in session workspace uploader: " + workspaceUploader,
						e);
				throw new Exception("An error occurred in the upload. Try again");
			}
		} else
			throw new Exception("An error occurred in the upload. Workspace Uploader not found. Abort and try again");
	}

	/**
	 * Creates the stream to temp file.
	 *
	 * @param request           the request
	 * @param workspaceUploader the workspace uploader
	 * @param in                the in
	 * @param username          the username
	 * @param clientUploadKey   the client upload key
	 * @param fileName          the file name
	 * @param fileExtension     the file extension
	 * @return the workspace uploader item
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public FileUploadingState createStreamToTempFile(HttpServletRequest request, FileUploadingState workspaceUploader,
			InputStream in, String username, String clientUploadKey, String fileName, String fileExtension)
			throws IOException {

		if (fileExtension == null)
			fileExtension = "";
		if (username == null)
			username = "";
		try {
			workspaceUploader.setStatusDescription("Uploading...");
			workspaceUploader.setUploadStatus(UPLOAD_STATUS.IN_PROGRESS);
			saveWorkspaceUploaderInSession(workspaceUploader, request.getSession());
			String uniqueFileName = String.format("%s_%s", clientUploadKey, fileName);
			File tempFile = File.createTempFile(uniqueFileName, fileExtension);
			tempFile.deleteOnExit();
			// File tempFile = File.createTempFile(uniqueFileName, fileExtension);
			// File tempFile = tempFilePath.toFile();
			FileOutputStream out = new FileOutputStream(tempFile);
			IOUtils.copy(in, out);
			workspaceUploader.getFile().setTempSystemPath(tempFile.getAbsolutePath());
			workspaceUploader.setStatusDescription("File uploaded correclty");
			workspaceUploader.setUploadStatus(UPLOAD_STATUS.COMPLETED);
			saveWorkspaceUploaderInSession(workspaceUploader, request.getSession());
			saveFileUplodedInSession(workspaceUploader.getFile(), request.getSession());
			logger.info("File uploaded at: " + tempFile.getAbsolutePath());
		} catch (Exception e) {
			workspaceUploader.setStatusDescription("Error on uploading: " + fileName + ". " + e.getMessage());
			workspaceUploader.setUploadStatus(UPLOAD_STATUS.FAILED);
		} finally {
			try {
				WsUtil.setErasableWorkspaceUploaderInSession(request, workspaceUploader.getIdentifier());
			} catch (Exception e2) {
				logger.error("Error during setErasableWorkspaceUploaderInSession session update: ", e2);
			}
		}
		return workspaceUploader;

	}

	/**
	 * Save file uploded in session.
	 *
	 * @param fileUploaded the file uploaded
	 * @param httpSession  the http session
	 */
	private void saveFileUplodedInSession(FileUploaded fileUploaded, HttpSession httpSession) {
		if (fileUploaded != null) {
			List<FileUploaded> listFiles = (List<FileUploaded>) httpSession
					.getAttribute(ConstantsMPFormBuilder.FILE_UPLOADED_SESSION_ATTR);

			if (listFiles == null)
				listFiles = new ArrayList<FileUploaded>();

			listFiles.add(fileUploaded);
			httpSession.setAttribute(ConstantsMPFormBuilder.FILE_UPLOADED_SESSION_ATTR, listFiles);
			logger.info("Added: " + fileUploaded + " in session");
		}
	}

	// TEST TIME
	/**
	 * Prints the start time.
	 *
	 * @return the long
	 */
	public static Long printStartTime() {
		Long startTime = System.currentTimeMillis();
		logger.debug("Start time: " + startTime);
		return startTime;
	}

	// TEST TIME
	/**
	 * Prints the elapsed time.
	 *
	 * @param startTime the start time
	 */
	public static void printElapsedTime(long startTime) {
		Long endTime = System.currentTimeMillis() - startTime;
		String time = String.format("%d msc %d sec", endTime, TimeUnit.MILLISECONDS.toSeconds(endTime));
		logger.debug("Elapsed Time: " + time);
	}

	/**
	 * Send error.
	 *
	 * @param response the response
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void sendError(HttpServletResponse response, String message) throws IOException {
		try {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			HandlerResultMessage resultMessage = HandlerResultMessage.errorResult(message);
			response.getWriter().write(resultMessage.toString());
			// 5.6 Closure of Response Object:
			// When a response is closed, the container must immediately flush all remaining
			// content in the response buffer to the client
//			response.flushBuffer();
		} catch (IOException e) {
			logger.warn("IOException class name: " + e.getClass().getSimpleName());
			if (e.getClass().getSimpleName().equals("ClientAbortException"))
				logger.warn("Skipping ClientAbortException: " + e.getMessage());
			else
				throw e; // Sending Exceptions
		}
	}

	/**
	 * Send session expired.
	 *
	 * @param response the response
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void sendSessionExpired(HttpServletResponse response, String message) throws IOException {
		try {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			HandlerResultMessage resultMessage = HandlerResultMessage.sessionExpiredResult(message);
			response.getWriter().write(resultMessage.toString());
			// 5.6 Closure of Response Object:
			// When a response is closed, the container must immediately flush all remaining
			// content in the response buffer to the client
//			response.flushBuffer();
		} catch (IOException e) {
			logger.warn("IOException class name: " + e.getClass().getSimpleName());
			if (e.getClass().getSimpleName().equals("ClientAbortException"))
				logger.warn("Skipping ClientAbortException: " + e.getMessage());
			else
				throw e; // Sending Exceptions
		}
	}

	/**
	 * Send message.
	 *
	 * @param response the response
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void sendMessage(HttpServletResponse response, String message) throws IOException {
		try {
			response.setStatus(HttpServletResponse.SC_ACCEPTED);
			message = message.replaceAll(":", "");
			HandlerResultMessage resultMessage = HandlerResultMessage.okResult(message);
			response.getWriter().write(resultMessage.toString());
			// 5.6 Closure of Response Object:
			// When a response is closed, the container must immediately flush all remaining
			// content in the response buffer to the client
//			response.flushBuffer();
		} catch (IOException e) {
			logger.warn("IOException class name: " + e.getClass().getSimpleName());
			if (e.getClass().getSimpleName().equals("ClientAbortException"))
				logger.warn("Skipping ClientAbortException: " + e.getMessage());
			else
				throw e; // Sending Exceptions
		}
	}

	/**
	 * Send warn message.
	 *
	 * @param response the response
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void sendWarnMessage(HttpServletResponse response, String message) throws IOException {
		try {
			response.setStatus(HttpServletResponse.SC_ACCEPTED);
			HandlerResultMessage resultMessage = HandlerResultMessage.warnResult(message);
			response.getWriter().write(resultMessage.toString());
			// 5.6 Closure of Response Object:
			// When a response is closed, the container must immediately flush all remaining
			// content in the response buffer to the client
//			response.flushBuffer();
		} catch (IOException e) {
			logger.warn("IOException class name: " + e.getClass().getSimpleName());
			if (e.getClass().getSimpleName().equals("ClientAbortException"))
				logger.warn("Skipping ClientAbortException: " + e.getMessage());
			else
				throw e; // Sending Exceptions
		}
	}

	/**
	 * Mark the current upload process to be canceled.
	 *
	 * @param session         the session
	 * @param clientUploadKey the client upload key
	 * @return true, if successful
	 */
	public boolean cancelUpload(HttpSession session, String clientUploadKey) {
		logger.debug("UPLOAD-SERVLET (" + session.getId() + ") cancelling Upload: " + clientUploadKey);
		AbstractUploadProgressListener listener = getCurrentListener(session, clientUploadKey);
		if (listener != null && !listener.isCanceled()) {
			logger.info("CancelUpload listener is " + listener.toString());
			listener.setException(new UploadCanceledException());
			return true;
		} else {
			logger.info("Skipping cancel upload: listener is null or is cancel");
			return false;
		}
	}

	/**
	 * Get the listener active in this session.
	 *
	 * @param session         the session
	 * @param clientUploadKey the client upload key
	 * @return the listener active
	 */
	protected AbstractUploadProgressListener getCurrentListener(HttpSession session, String clientUploadKey) {
		if (isAppEngine()) {
			return MemoryUploadListener.current(session.getId(), clientUploadKey);
		} else {
			return UploadProgressListener.current(session, clientUploadKey);
		}
	}

	/**
	 * Just a method to detect whether the web container is running with appengine
	 * restrictions.
	 *
	 * @return true if the case of the application is running in appengine
	 */
	public boolean isAppEngine() {
		return appEngine;
	}

	/**
	 * Create a new listener for this session.
	 *
	 * @param session         the session
	 * @param clientUploadKey the client upload key
	 * @return the appropriate listener
	 */
	protected AbstractUploadProgressListener createNewListener(HttpSession session, String clientUploadKey) {
		if (isAppEngine()) {
			return new MemoryUploadListener(session, clientUploadKey, 0, 100);
		} else {
			return new UploadProgressListener(session, clientUploadKey, 0, 100);
		}
	}

	/**
	 * Gets the content length.
	 *
	 * @param request the request
	 * @return the content length
	 */
	private long getContentLength(HttpServletRequest request) {
		long size = -1;
		try {
			size = Long.parseLong(request.getHeader(FileUploadBase.CONTENT_LENGTH));
		} catch (NumberFormatException e) {
		}
		return size;
	}

	/**
	 * Remove the listener active in this session.
	 *
	 * @param session         the session
	 * @param clientUploadKey the client upload key
	 */
	protected void removeCurrentListener(HttpSession session, String clientUploadKey) {
		logger.debug("RemoveCurrentListener: " + clientUploadKey);
		AbstractUploadProgressListener listener = getCurrentListener(session, clientUploadKey);

		if (listener != null) {
			logger.debug("Removing listener: " + listener.getClientUploadKey());
			listener.remove();
		} else
			logger.warn("Listener " + clientUploadKey + "is null");
	}
}
