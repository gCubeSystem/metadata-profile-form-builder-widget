package org.gcube.portlets.widgets.mpformbuilder.shared.metadata;

/**
 * The Enum DataTypeWrapper.
 * @see org.gcube.common.metadataprofilediscovery.jaxb.DataType
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 6, 2020
 */
public enum DataTypeWrapper {

	/** The String. */
	String,
	
	/** The Time. */
	Time,
	
	/** The Time interval. */
	Time_Interval,
	
	/** The Times list of. */
	Times_ListOf,
	
	/** The Text. */
	Text,
	
	/** The Boolean. */
	Boolean,
	
	/** The Number. */
	Number,
	
	/** The Geo JSON. */
	GeoJSON,
	
	/** The File. */
	File;

	/**
	 * Value as String.
	 * @return the string
	 */
	public String value() {
		return name();
	}
}
