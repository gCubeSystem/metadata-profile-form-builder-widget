package org.gcube.portlets.widgets.mpformbuilder.client;

import java.util.List;

import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadingState;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MetadataProfileFormBuilderServiceAsync {

	/**
	 * Utility class to get the RPC Async interface from client-side code
	 */
	public static final class Util {
		private static MetadataProfileFormBuilderServiceAsync instance;

		public static final MetadataProfileFormBuilderServiceAsync getInstance() {
			if (instance == null) {
				instance = (MetadataProfileFormBuilderServiceAsync) GWT.create(MetadataProfileFormBuilderService.class);
			}
			return instance;
		}

		private Util() {
			// Utility class should not be instantiated
		}
	}

	/**
	 * GWT-RPC service asynchronous (client-side) interface
	 * 
	 * @see org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderService
	 */

	void getProfilesInTheScope(String scope, String genericResourceSecondaryType,
			AsyncCallback<List<MetaDataProfileBean>> callback);

	void getProfileForMetadata(String metadata, AsyncCallback<MetaDataProfileBean> callback);

	void getUploadStatus(String clientUploadKey, AsyncCallback<FileUploadingState> asyncCallback);

	void purgeFilesUploaded(AsyncCallback<Integer> callback);

	void getProfilesInTheScopeForName(String scope, String genericResourceSecondaryType, String resourceName,
			AsyncCallback<List<MetaDataProfileBean>> callback);
}
