package org.gcube.portlets.widgets.mpformbuilder.shared.metadata;

import java.util.List;

public interface UpdatableField {

	void setCurrentValues(String... values);

	List<String> getCurrentValues();
}
