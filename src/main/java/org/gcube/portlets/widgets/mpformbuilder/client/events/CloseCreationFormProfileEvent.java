package org.gcube.portlets.widgets.mpformbuilder.client.events;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class CloseCreationFormProfileEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 12, 2024
 */
public class CloseCreationFormProfileEvent extends GwtEvent<CloseCreationFormProfileEventHandler>{
	
	public static Type<CloseCreationFormProfileEventHandler> TYPE = new Type<CloseCreationFormProfileEventHandler>();
	
	/**
	 * Instantiates a new close creation form profile event.
	 */
	public CloseCreationFormProfileEvent() {
		super();
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<CloseCreationFormProfileEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(CloseCreationFormProfileEventHandler handler) {
		handler.onClose(this);
	}

}
